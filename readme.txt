All functions are included in "Codes". The main code is "mission.py".
Necessary data to run the tool is included in "Data".
"Validation" includes results from SPENVIS of some sample missions for comparison.
PLEASE DO NOT MODIFY ANY OF THESE.

In order to use the tool, the input file with the required contents and format (see manual and "mission.py") must be located in the "Input" folder.
Then, it is only "mission.py" that needs to be run. If selected (in the input file), the output files are stored in "Output".