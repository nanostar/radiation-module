"""
Solar activity module. It includes the necessary functions to determine the epochs and activity of the solar cycles spanned during the mission.
"""

import numpy as np
import otherfuns as funs

######################################################################################################################################################

def cycledata():
    """
    Solar cycle data function. Returns solar cycle data for cycles nº 18 through nº 26 [Solar Influences Data Analysis Center (SIDC) - Sunspot Index
    and Long-term Solar Observations (SILSO), SPENVIS].
    Output:
        - n:      solar cycle numbers (1D array of ints)
        - solmin: epochs of solar cycle minimum [yr] (1D array of floats [n])
        - solmax: epochs of solar cycle maximum [yr] (1D array of floats [n])
    """
    n = np.arange(19, 27)
    solmin = np.array([1954.5, 1964.5, 1976.5, 1986.5, 1996.5, 2008.5, 2020.0, 2031.0])
    solmax = np.array([1957.5, 1969.5, 1979.5, 1989.5, 2000.5, 2014.5, 2024.4, 2035.4])
    
    return n, solmin, solmax

######################################################################################################################################################

def actdata():
    """
    Solar activity data function. Returns solar activity data for cycles nº 18 through nº 26 [Solar Influences Data Analysis Center (SIDC) - Sunspot
    Index and Long-term Solar Observations (SILSO), SPENVIS].
    Output:
        - T: epoch data [yr] (1D array of floats)
        - W: sunspot number data in function of epoch (1D arrays of float [T])
    """
    T = np.linspace(1954.5, 2017.5, 64)
    W = np.array([6.6, 54.2, 200.7,	269.3, 261.7, 225.1, 159., 76.4, 53.4, 39.9, 15., 22., 66.8, 132.9, 150., 149.4, 148., 94.4, 97.6, 54.1, 49.2,
                  22.5, 18.4, 39.3, 131., 220.1, 218.9, 198.9, 162.4, 91., 60.5, 20.6, 14.8, 33.9, 123., 211.1, 191.8, 203.3, 133., 76.1, 44.9, 25.1,
                  11.6, 28.9, 88.3, 136.3, 173.9, 170.4, 163.6, 99.3, 65.3, 45.8, 24.7, 12.6, 4.2, 4.8, 24.9, 80.8, 84.5, 94., 113.3, 69.8, 39.8,
                  21.7])
    
    return T, W

######################################################################################################################################################

def cycle(T, N=None):
    """
    Solar cycle function. Returns the solar cycle number, maximum epoch and minimum epoch for the given epoch.
    Input:
        - T:       epoch [yr] (float)
        - N:       solar cycle number (int) (if given, epoch is ignored, returning data of that cycle) (defaults to None)
    Output:
        - N:       solar cycle number (int)
        - Tsolmin: epoch of solar minimum (float)
        - Tsolmax: epoch of solar maximum (float)
    """
    if N is None:
        ndata, Tsolmindata, Tsolmaxdata = cycledata()
        i = np.where(Tsolmindata <= T)[0][-1]
        N, Tsolmin, Tsolmax = ndata[i], Tsolmindata[i], Tsolmaxdata[i]
        
    else:
        n, solmindata, solmaxdata = cycledata()
        Tsolmin, Tsolmax = solmindata[N-n[0]], solmaxdata[N-n[0]]
    
    return N, Tsolmin, Tsolmax

######################################################################################################################################################

def activity(T):
    """
    Solar activity function. Returns the cycle-minimum, cycle-maximum and average instantaneous sunspot number for the given epoch.
    Input:
        - T:    epoch [yr] (float)
    Output:
        - Wmin: solar cycle minimum sunspot number (float)
        - Wmax: solar cycle maximum sunspot number (float)
        - W:    yearly-average instantaneous sunspot number (float)
    """
    Tdata, Wdata = actdata()
    _, Tsolmin, Tsolmax = cycle(T)
    Wmin, Wmax = float(Wdata[Tdata == Tsolmin]), float(Wdata[Tdata == Tsolmax])
    W = funs.loginterp(Tdata, Wdata, T, kind='semilogy')
    
    return Wmin, Wmax, W

######################################################################################################################################################

def solmaxtime(Ti, Tf):
    """
    Solar-maximum exposure time function. Returns the total time in solar maximum conditions (from 2.5 yr before to 4.5 yr after the SOLMAX date) for
    the given epochs.
    Input:
        - Ti, Tf:  initial, final epochs [yr] (floats)
    Output:
        - Tsolmax: time in solar maximum conditions [yr] (float)
    """
    Tsolmax = 0.0
    T0 = Ti
    n0, _, solmax0 = cycle(T0)
    n1, solmin1, solmax1 = cycle(None, n0 + 1)
    while Tf >= solmin1:
        # Current cycle
        if T0 - solmax0 <= -2.5:
            Tsolmax += 7.0
        elif -2.5 < T0 - solmax0 <= 0.0:
            Tsolmax += abs(T0 - solmax0) + 4.5
        elif 0.0 < T0 - solmax0 < 4.5:
            Tsolmax += 4.5 - (T0 - solmax0)
        else:
            if T0 != Ti:
                Tsolmax += 7.0
        
        # Next cycle
        if n1 == 26:
            break
        else:
            T0 = solmin1
            n0, solmax0 = n1, solmax1
            n1, solmin1, solmax1 = cycle(None, n0 + 1)
    
    # Last cycle
    if -2.5 <= Tf - solmax0 <= 0.0:
        Tsolmax += Tf - T0 if (-2.5 <= T0 - solmax0 <= 0.0) else 2.5 - abs(Tf - solmax0)
    elif 0.0 < Tf - solmax0 <= 4.5:
        Tsolmax += Tf - T0 if (-2.5 <= T0 - solmax0 <= 4.5) else 2.5 + Tf - solmax0
    elif Tf - solmax0 > 4.5:
        if T0 - solmax0 < -2.5:
            Tsolmax += 7.0
        elif -2.5 <= T0 - solmax0 <= 4.5:
            Tsolmax += 4.5 - (T0 - solmax0)
            
    return Tsolmax

######################################################################################################################################################