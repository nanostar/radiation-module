"""
Test cases module. Function to run the included sample missions and compare the results with SPENVIS.
"""

import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mission
import otherfuns as funs

######################################################################################################################################################

def testcase(ID):
    """
    MISSION
    """
    "Mission input"
    s = ['GEO', 'ISS', 'MEO', 'Mars', 'UPMSAT'].index(ID)
    name = ['GEO 2000 - 2015', 'LEO (ISS) 2000 - 2025', 'MEO (Molniya-like) 2005 - 2025', 'Interplanetary (Mars) 2020 - 2030',
            'LEO (UPMSAT) 2020 - 2025'][s]
    
    "Mission run"
    T, t, r, rad, dmg = mission.run(name)
    tal = dmg['tsh (mm Al)']
    dur = T[-1] - T[0]
    
    
    """
    SOLAR PARTICLES
    """
    "PROTONS"
    "Tool"
    spr = rad['SPR']
    ths = [np.where(tal == th)[0][0] for th in [0.1, 1., 10.]]
    
    "SPENVIS/OMERE"
    SPR = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/SPR.csv", header=1)
    
    "Plot"
    clrs = ['k', 'b', 'g', 'r']
    lbls = ['unshielded', '0.1 mm', '1 mm', '10 mm']
    iflut = [funs.integralfrom(spr.dfluencet[th], spr.E) for th in ths]
    funs.plot(plt.loglog, [spr.E]*4, [spr.ifluence] + iflut, clrs, ['-']*4, lbls,
              ['Energy [MeV]', 'Integral fluence [cm$^{-2}$]'], title='Solar protons')
    [plt.scatter(SPR.values[:,0], SPR.values[:,[1, 4, 5, 6][k]], s=10, marker='s', color=clrs[k]) for k in range(len(clrs))]
    plt.xlim(1e-1, 5e2)
    
    
    
    """
    TRAPPED PARTICLES
    """
    "Tool"
    tpr = rad['TPR']
    tel = rad['TEL']
    
    "SPENVIS/OMERE"
    TPR = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/TPR.csv", header=1)
    TEL = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/TEL.csv", header=1)
    
    if r is not None:
        "Plot"
        clrs = ['b', 'g']
        lbls = ['Trapped protons', 'Trapped electrons']
        funs.plot(plt.loglog, [tpr.E, tel.E], [tpr.dfluence, tel.dfluence], clrs, ['-']*2, lbls,
                  ['Energy [MeV]', 'Differential fluence [cm$^{-2}$ MeV$^{-1}$]'], title='Trapped particles (unshielded)')
        plt.scatter(TPR.values[:,0], TPR.values[:,2]*dur*365*24*3600, s=20, marker='s', color=clrs[0], label=lbls[0])
        plt.scatter(TEL.values[:,0], TEL.values[:,2]*dur*365*24*3600, s=20, marker='s', color=clrs[1], label=lbls[1])
    
    
    
    """
    GALACTIC COSMIC RAYS
    """
    "Tool"
    h, he, c = rad['H gcr'], rad['He gcr'], rad['C gcr']
    
    "SPENVIS/OMERE"
    GCR = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/GCR.csv", header=1)
    
    "Plot"
    clrs = ['k', 'b', 'g']
    lbls = ['H', 'He', 'C']
    funs.plot(plt.loglog, [h.E, he.E, c.E], [h.dfluence, he.dfluence, c.dfluence], clrs, ['-']*3, lbls,
              ['Energy [MeV/nucl]', 'Differential fluence [cm$^{-2}$ MeV$^{-1}$]'], title='Galactic cosmic rays (unshielded)')
    [plt.scatter(GCR.values[:,0], GCR.values[:,k+1]/1e4*dur*365*24*3600*4*math.pi, s=20, marker='s', color=clrs[k]) for k in range(len(clrs))]
    
    
    
    """
    IONISING DOSE
    """
    "Tool"
    tid = dmg['TID']
    
    "SPENVIS"
    ID = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/ID.csv", header=1)
    
    "Plot"
    clrs = ['k', 'g', 'r', 'b', 'y']
    lbls = ['Total', 'Trapped electrons', 'Bremsstrahlung', 'Trapped protons', 'Solar protons']
    funs.plot(plt.loglog, [tal]*5, [tid, tel.idose, tel.idoseBR, tpr.idose, spr.idose], clrs, ['-']*5, lbls,
              ['Al-equivalent shield thickness [mm]', 'Ionising dose [Gy]'], title='Ionising dose in Si (spherical shielding)')
    [plt.scatter(ID.values[:,0], ID.values[:,k+1]/100, s=20, marker='s', color=clrs[k]) for k in range(len(clrs))]
    plt.xlim(0.025, 20.5); plt.ylim(bottom=tid.min()/100)
    
    
    
    """
    DISPLACEMENT DOSE
    """
    "Tool"
    tdd = dmg['TDD']
    
    "SPENVIS"
    DD = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/DD.csv", header=1)
    
    "Plot"
    clrs = ['k', 'g', 'b']
    lbls = ['Total', 'Electrons', 'Protons']
    funs.plot(plt.semilogy, [tal]*3, [tdd, tel.ddose, tpr.ddose + spr.ddose], clrs, ['-']*3, lbls,
              ['Al-equivalent shield thickness [mm]', 'Displacement dose [MeV g$^{-1}$]'], title='Displacement dose in Si (spherical shielding)')
    plt.scatter(DD.values[:,0], DD.values[:,1], s=20, marker='s', color=clrs[-1])
    plt.xlim(0.025, 20.5); plt.ylim(bottom=tdd.min()/100)
    
    
    
    """
    SOLAR CELL DEGRADATION
    """
    "Tool"
    tcg, pmj, psj = dmg['tcg (um SiO2)'], dmg['Psc'], dmg['Psj']
    
    "SPENVIS"
    SCDEGR = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/SCDEGR.csv", header=1)
    
    "Plot"
    clrs = ['g', 'b']
    lbls = ['Single junction GaAs/Ge', 'Emcore ATJ']
    
    funs.plot(plt.plot, [tcg]*2, [psj*100, pmj*100], clrs, ['-']*2, lbls,
              ['Coverglass thickness [$\mu$m]', 'Power ratio (EOL-to-BOL) [%]'], title='Solar cell degradation')
    [plt.scatter(SCDEGR.values[:,0], SCDEGR.values[:,k+1], s=20, marker='s', color=clrs[k]) for k in range(len(clrs))]
    
    
    
    """
    SINGLE EVENT EFFECTS
    """
    "Tool"
    ni, npr, nd = dmg['Ni'], dmg['Np'], dmg['Nd']
    
    "SPENVIS"
    SEU = pd.read_csv("../Validation/Full missions/" + name + "/SPENVIS data/SEU.csv", header=1)
    
    "SEU Plot"
    clrs = ['r', 'b']
    lbls = ['Direct ionisation (ions)', 'Nuclear reactions (protons)']
    funs.plot(plt.loglog, [tal]*2, [ni/(dur*365), npr/(dur*365)], clrs, ['-']*2, lbls,
              ['Al-equivalent shield thickness [mm] ', 'Average SEU rate [bit$^{-1}$ day$^{-1}$]'],
              title='SEUs in Si bipolar transistor (spherical shielding)')
    [plt.scatter(SEU.values[:,0], SEU.values[:,k+1]/(dur*365), s=20, marker='s', color=clrs[k]) for k in range(len(clrs))]
    
    "Destructive SEE Plot"
    funs.plot(plt.loglog, [tal], [nd*100], ['k'], ['-'], [None],
              ['Al-equivalent shield thickness [mm] ', 'Probability of destructive SEE [%]'],
              title='Destructive SEEs in Si component (spherical shielding)')

######################################################################################################################################################