"""
Mission module (main function). It calls all other modules in the right order to run the simulation of a full mission, including all radiation sources
and effects.
"""

import os
import ast
import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import orbits
import radsrc
import shielding
import radeff
import otherfuns as funs

######################################################################################################################################################

def run(infilename):
    """
    Run full mission simulation function. Mission parameters and options are input through a .txt file with Python syntax. Main results are output
    as .csv files if desired.
    INPUT:
    Input file <infilename>.txt in the "Input" folder, with lines:
    1. Type of input: 'basic' or 'advanced'. The basic input option allows to run the mission with advanced parameters set to default (see below).
    
    2. Plot results: True or False. If True, plots main results of the mission. Figures are NOT saved automatically.
    
    3. Save output: True or False. If True, saves main results as .csv files to a folder named as the input file, inside the "Output" folder.
    
    4. Mission trajectory data: type of trajectory, trajectory elements, number of orbits, orbital resolution:
                                - Type of trajectory: 'general', 'GNSS'/'NAV', 'GEO', 'interplanetary' or 'custom'
                                - Trajectory elements: + General, GNSS/NAV: ['ae', a, e, i, Omega, omega] or ['h', hp, ha, i, Omega, omega]:
                                                                            * 'ae', 'h': input semi-major axis and eccentricity or perigee and apogee
                                                                                         altitudes
                                                                            * a, e:      semi-major axis and eccentricity [km]
                                                                            * hp, ha:    perigee and apogee altitudes [km]
                                                                            * i:         inclination [deg]
                                                                            * Omega:     longitude of ascending node [deg]
                                                                            * omega:     longitude of perigee [deg]
                                                       
                                                       + GEO:               phi: longitude [deg]
                                                       
                                                       + interplanetary: ['ae', a, e, i, Omega, omega, nu0] or ['r', rp, ra, i, Omega, omega, nu0]:
                                                                         * 'ae', 'r': input semi-major axis and eccentricity or perihelium and
                                                                                      aphelium radii
                                                                         * a, e:      heliocentric semi-major axis and eccentricity [km]
                                                                         * hp, ha:    perihelium and aphelium radii [AU]
                                                                         * i:         heliocentric inclination [deg]
                                                                         * Omega:     heliocentric longitude of ascending node [deg]
                                                                         * omega:     heliocentric longitude of perigee [deg]
                                                                         * nu0:       initial true anomaly [deg]
                                                       
                                                       + custom: '<tlefilename>': name of input TLE (Two-Line Element set) file (as string). The TLE
                                                                 file must be located in "Data/Trajectories/".
                                - Number of orbits: number of revolutions to be actually simulated, representative of the real conditions (int). Set
                                                    to None for GEO and interplanetary orbits.
                                - Orbital resolution: number of points (in true anomaly) per orbit to be actually simulated (int). Set to None for GEO
                                                      and interplanetary orbits.
    
    5. Time data: mission start epoch [yr], mission duration [yr] (both specified as decimal years, e.g. start 10/02/2020 --> 2020.112).
                  NOTE: mission epochs must lie between 1954.5 and 2042.0.
    
    NOTE: Lines 1-5 make up the basic input and are necessary to run the mission. The following lines define advanced input parameters, thus need not
          be defined if the 'basic' type of input is selected.
    
    6. Solar proton model: time in solar maximum, confidence level, energy resolution, heliocentric correction:
                           - Time in solar maximum: total time of the mission spent in conditions of high solar activity (2.5 yr before - 4.5 yr after
                                                    SOLMAX date) (float) [yr]. The mission proton fluence is computed for the time in solar maximum,
                                                    not the total mission duration. If None, it is computed automatically for the given mission
                                                    epochs. Defaults to: None (solar activity computed according to mission epochs).
                           - Confidence level: probability that the total mission proton fluence does NOT exceed the computed value (float) [%].
                                               Defaults to: 95% confidence.
                           - Energy resolution: number of solar proton energy points (int). Defaults to: 100 energy points.
                           - Heliocentric correction: use mission-averaged heliocentric distance (True) or 1 AU regardless of the trajectory (False).
                                                      Defaults to: True (include heliocentric distance correction).
    
    7. Trapped particle model: energy resolution: number of trapped particle energy points (int). Defaults to: 100 energy points.
    
    8. Galactic cosmic rays model: standard deviations, energy resolution:
                                   - Standard deviations: number of standard deviations (int). Defaults to: 2 std devs.
                                   - Energy resolution: number of galactic cosmic ray energy points (int). Defaults to: 50 energy points.
    
    9. Geomagnetic shielding model: apply, stormy:
                                    - Apply: apply (True) or not (False) geomagnetic shielding. Defaults to: True (apply)
                                    - Stormy: include stormy magnetosphere correction (weaker geomagnetic shielding). Defaults to: False (not stormy).
    
    10. Spacecraft shielding: (geometry, thicknesses) or None:
                             - Geometry: shielding configuration: 'planar' or 'spherical'. Defaults to: 'spherical'.
                             - Thicknesses: aluminium-equivalent shielding material thicknesses of the spacecraft: [<thicknesses>] [mm]
                                            (list of floats). Defaults to: [0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.5, 0.7, 1., 2., 3., 5., 7.,
                                            10., 20., 30., 50.].
                             If None, default input are used.
    
    11. Damage target: material for radiation damage assessment: 'Si' or 'GaAs'. Defaults to: 'Si' (silicon).
    
    12. Solar cell data: (name, tcg, (Cp, Dxp, Ce, Dxe, n)) or None:
                         - Name: name of solar cell (str). Degradation data of three families of solar cells are included: 'GaAs SJ', 'Emcore 3J',
                                 'Spectrolab 3J'. Defaults to: 'Emcore 3J' (data included). Results for single-junction cells ('GaAs SJ') are always
                                 computed for comparison.
                         - tcg: coverglass (SiO2) thicknesses of the solar cell: [<thicknesses>] [um] (list of floats). Defaults to: [25., 50., 75.,
                                100., 150., 200., 300., 500., 700., 1000.].
                         - (Cp, Dxp, Ce, Dxe, n): experimental degradation parameters for Displacement Damage Dose (DDD) method (see manual). Defaults
                                                  to: None (data of 'Emcore 3J' cells).
                         If None, default input are used.
    
    13. Device data for SEE: (name, (dims, L0, L0d, pfitp)) or None:
                             - Name: name of device (str). Defaults to: '93L422AM bipolar transistor' (Si), 'MESFET 1K SRAM' (GaAs) (see manual).
                             - (dims, L0, L0d, pfitp): experimental single event parameters (see manual):
                                                       + dims: dimensions (length, width, height) [um] (floats)
                                                       + L0: upset LET threshold [MeV cm2 g-1] (float)
                                                       + L0d: destructive LET threshold [MeV cm2 g-1] (float)
                                                       + pfitp: proton cross section Bendel fit parameters: (A [MeV], sigmasatp [cm2 bit-1]) (tuple of
                                                                floats)
                             If None, default input are used.
    
    NOTE: the input file must contain all input parameters as specified in Python syntax: floats must be written with decimal point (or in
          exponential form); integers with no decimal point; strings with quotes; True, False and None without quotes, etc.
    
    OUTPUT:
    Returns:
        - T:   mission epochs [yr] (1D array of floats)
        - t:   orbital time [s] (1D array of floats)
        - r:   orbital position vector [spherical geographic] [m, rad, rad] in time (2D array of floats: [t, [r, theta, phi]]) 
        - rad: radiation particles (dictionary of RadPart objects)
        - dmg: radiation damage (dictionary of radiation effects)
    
    If save output is True (line 3 in input file), the main results are saved as .csv files to folder <infilename> inside "Output" folder:
        - spr:       solar proton radiation incident differential fluence and mission-averaged exposure factor in function of kinetic energy
        - tpr:       trapped proton radiation incident differential fluence in function of kinetic energy
        - tel:       trapped electron radiation incident differential fluence in function of kinetic energy
        - gcr_dflu:  galactic cosmic rays incident differential fluence of all particles in function of kinetic energy
        - gcr_mfexp: galactic cosmic rays mission-averaged exposure factor of all particles in function of kinetic energy
        - id:        ionising dose in target material in function of shielding thickness
        - dd:        displacement dose in target material in function of shielding thickness
        - scdegr:    solar cell power degradation (EOL-to-BOL ratio) for GaAs SJ and the input solar cell in function of coverglass thickness
        - see:       average soft error rate and probability of destructive effect in function of shielding thickness
    """
    np.seterr(all='ignore')
    
    START = time.time()
    print('MISSION START')
    
    """
    MISSION DEFINITION
    """
    print('Reading mission input... ', end='')
    
    "Input file"
    infile = open('../Input/' + infilename + '.txt', 'r')
    inrows = infile.readlines()
    infile.close()
    
    "Mission input"
    inptype, plot, output, trjdata, Tdata = [ast.literal_eval(row) for row in inrows[:5]]
    if inptype == 'advanced':
        spdata, tpdata, gcrdata, geomagshdata, shdata, dmgtarget, scdata, seedata = [ast.literal_eval(row) for row in inrows[5:]]
    else:
        spdata, tpdata, gcrdata = (None, 95, 100, True), (100), (2, 50)
        geomagshdata, shdata, dmgtarget = (True, False), None, 'Si'
        scdata, seedata = None, None
    
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    
    """
    TRAJECTORY
    """
    print('Generating trajectory... ', end='')
    
    "Mission epochs"
    T0, DeltaT = Tdata
    if T0 < 1954.5 or T0 + DeltaT > 2042.5:
        raise ValueError('Mission epochs must lie between 1954.5 and 2042.0')
    Nep = round(DeltaT/4)
    T = T0 + np.linspace(0., DeltaT, Nep + 1)
    
    "Trajectory data"
    trjtype, trjelems, Norb, orbres = trjdata
    
    "Orbital elements"
    if trjtype == 'interplanetary':
        rhmean = orbits.interplanetary(trjelems, T)
        t, r = np.array([0.]), None
    elif trjtype == 'custom':
        tau, t, r = orbits.custom(trjelems, Norb, orbres)
        rhmean = 1.
    else:
        tau, t, r = orbits.GEO(trjelems) if trjtype == 'GEO' else orbits.general(trjelems, Norb, orbres)
        rhmean = 1.
    
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    
    """
    RADIATION SOURCES
    """
    "Radiation particles dictionary"
    rad = {}
    
    "Solar particles"
    print('Generating solar particles... ', end='')
    Tsolmax, conflvl, Eres_sp, rhcorr = spdata
    rad.update(radsrc.SolarParticles(T, Tsolmax, rhmean, rhcorr, conflvl, dmgtarget, Eres_sp))
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    "Trapped particles"
    print('Generating trapped particles... ', end='')
    Eres_tp = tpdata
    rad.update(radsrc.TrappedParticles(T, trjtype, t, r, dmgtarget, Eres_tp))
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    "Galactic cosmic rays"
    print('Generating galactic cosmic rays... ', end='')
    nstdev, Eres_gcr = gcrdata
    rad.update(radsrc.GCRParticles(T, nstdev, dmgtarget, Eres_gcr))
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    
    """
    SHIELDING
    """
    print('Applying geomagnetic shielding and Earth shadow... ', end='')
    "GEOMAGNETIC"
    "Conditions"
    magsh, stormy = (False, False) if trjtype in ['GEO', 'interplanetary'] else geomagshdata
    
    "Geomagnetic shielding"
    shielding.GeoShielding(T, t, r, trjtype, magsh, stormy, rad)
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    "MATERIAL"
    "Spacecraft shielding"
    print('Applying spacecraft shielding... ', end='')
    if shdata is None:
        shconf = 'spherical'
        tsh = np.array([0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.5, 0.7, 1., 2., 3., 5., 7., 10., 20., 30., 50.]) # [mm]
    else:
        shconf, tsh = shdata[0], np.array(shdata[1])
        if 1. not in tsh:
            tsh = np.sort(np.append(tsh, 1.))
    shielding.MaterialShielding(tsh/10*2.7, shconf, rad)
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    
    """
    RADIATION EFFECTS
    """    
    "Radiation damage dictionary"
    dmg = {'tsh (mm Al)': tsh, 'shconf': shconf, 'mat': dmgtarget}
    
    
    "Ionising dose"
    print('Computing ionising dose... ', end='')
    TID = radeff.IonisingDose(DeltaT, dmgtarget, tsh, shconf, rad['SPR'], rad['TPR'], rad['TEL'])
    dmg['TID'] = TID
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    "Displacement dose"
    print('Computing displacement dose... ', end='')
    TDD = radeff.DisplacementDose(tsh, rad['SPR'], rad['TPR'], rad['TEL'])
    dmg['TDD'] = TDD
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    "Solar cell degradation"
    print('Computing solar cell degradation... ', end='')
    if scdata is None:
        scname, tcg = 'Emcore 3J', np.array([25., 50., 75., 100., 150., 200., 300., 500., 700., 1000.])
    else:
        scname, tcg = scdata[0], np.array(scdata[1])
    dmg['tcg (um SiO2)'] = tcg
    scdegrfun = radeff.SolarCellDegradation(tcg, rad['SPR'], rad['TPR'], rad['TEL'])
    if scname == 'Emcore 3J':
        Cp, Dxp, Ce, Dxe, n = 0.199, 1.2e9, 0.199, 1.2e9/0.17, 1.8
    elif scname == 'Spectrolab 3J':
        Cp, Dxp, Ce, Dxe, n = 0.3, 3e9, 0.3, 3e9/0.3, 1.6
    else:
        Cp, Dxp, Ce, Dxe, n = scdata[2]
    Psc, Psj = scdegrfun(Cp, Dxp, Ce, Dxe, n), scdegrfun(0.2904, 1.1e9, 0.363, 6.9e9, 1.647)
    dmg['Psc'], dmg['Psj'] = Psc, Psj
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    "Single event effects"
    print('Computing single event effects... ', end='')
    if seedata is None and dmgtarget == 'Si':
        devname, devdata = '93L422AM bipolar transistor', ((38.73, 38.73, 2.), 547., 5e4, (6.5, 3.136e-10))
    elif seedata is None and dmgtarget == 'GaAs':
        devname, devdata = 'MESFET 1K SRAM', ((20.0, 20.0, 2.), 770., 5e4, (17.6, 9.148e-11))
    else:
        devname, devdata = seedata
    Ni, Np, Nd = radeff.SingleEventEffects(rad, tsh, devdata)
    dmg['Ni'], dmg['Np'], dmg['Nd'] = Ni, Np, Nd
    print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    
    """
    PLOTS
    """
    if plot:
        print('Plotting results... ', end='')
        "Orbit"
        if trjtype != 'interplanetary' and len(t) > 1:
            fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
            ax1.plot(t/tau, r[:,0]/1e3 - 6371., 'k')
            ax2.plot(t/tau, 90 - np.degrees(r[:,1]), 'k')
            ax3.plot(t/tau, np.degrees(r[:,2]), 'k')
            ax1.set_ylabel('Altitude [km]'); ax2.set_ylabel('Latitude [deg]'); ax3.set_ylabel('Longitude [deg]')
            ax3.set_xlabel('Time (normalised to orbital period)')
            ax1.grid(True, which='both'); ax2.grid(True, which='both'); ax3.grid(True, which='both')
        
        
        "Solar and trapped radiation mission fluence"
        tAl1mm = np.where(tsh==1.)[0][0]
        funs.plot(plt.loglog, [rad['SPR'].E, rad['TPR'].E, rad['TEL'].E]*2, [rad['SPR'].dfluence, rad['TPR'].dfluence, rad['TEL'].dfluence,
                  rad['SPR'].dfluencet[tAl1mm], rad['TPR'].dfluencet[tAl1mm], rad['TEL'].dfluencet[tAl1mm]],
                  ['y', 'b', 'g']*2, ['-']*3 + [':']*3, ['Solar protons', 'Trapped protons', 'Trapped electrons',
                  'Solar protons (1 mm Al shield)', 'Trapped protons (1 mm Al shield)', 'Trapped electrons (1 mm Al shield)'],
                  ['Energy [MeV]', 'Differential fluence [cm$^{-2}$ MeV$^{-1}$]'],
                  title='Solar and trapped radiation mission fluence')
        mins = np.array([rad['SPR'].dfluence[-1], rad['TPR'].dfluence[-1], rad['TEL'].dfluence[-1]])
        maxs = np.array([rad['SPR'].dfluence.max(), rad['TPR'].dfluence.max(), rad['TEL'].dfluence.max()])
        plt.ylim(bottom=0.8*mins[mins > 0.].min(), top=1.2*maxs[maxs > 0.].max())
        
        
        "Galactic cosmic radiation fluences"
        funs.plot(plt.loglog, [rad['H gcr'].E, rad['He gcr'].E, rad['C gcr'].E],
                  [rad['H gcr'].dfluence, rad['He gcr'].dfluence, rad['C gcr'].dfluence], ['k', 'b', 'g'], ['-']*3, ['H', 'He', 'C'],
                  ['Energy [MeV/nucl]', 'Differential fluence [cm$^{-2}$ MeV$^{-1}$]'], title='Galactic cosmic radiation mission fluences')
        
        
        "Solar and galactic cosmic radiation exposure factors"
        if magsh is True:
            funs.plot(plt.semilogx, [rad['SPR'].E, rad['H gcr'].E, rad['He gcr'].E, rad['C gcr'].E],
              [rad['SPR'].mfexp, rad['H gcr'].mfexp, rad['He gcr'].mfexp, rad['C gcr'].mfexp], ['y', 'k', 'b', 'g'], ['-']*4,
              ['Solar protons', 'Galactic cosmic H', 'Galactic cosmic He', 'Galactic cosmic C'],
              ['Energy [MeV/nucl]', 'Average exposure factor'], title='Solar and galactic cosmic radiation exposure')
        
        
        "Ionising dose"
        funs.plot(plt.loglog, [tsh]*5, [TID, rad['TEL'].idose, rad['TEL'].idoseBR, rad['TPR'].idose, rad['SPR'].idose], ['k', 'g', 'r', 'b', 'y'],
                  ['-']*5, ['Total', 'Trapped electrons', 'Bremsstrahlung', 'Trapped protons', 'Solar protons'],
                  ['Al-equivalent shield thickness [mm]', 'Ionising dose [Gy]'],
                  title='Ionising dose in ' + dmgtarget + ' (' + shconf + ' shielding)')
        plt.ylim(bottom=TID.min()/1e2, top=1.2*TID.max())
        
        
        "Displacement dose"
        funs.plot(plt.loglog, [tsh]*4, [TDD, rad['TEL'].ddose, rad['TPR'].ddose, rad['SPR'].ddose], ['k', 'g', 'b', 'y'],
                  ['-']*4, ['Total', 'Trapped electrons', 'Trapped protons', 'Solar protons'],
                  ['Al-equivalent shield thickness [mm]', 'Displacement dose [MeV g$^{-1}$]'],
                  title='Displacement dose in ' + dmgtarget + ' (' + shconf + ' shielding)')
        plt.ylim(bottom=dmg['TDD'].min()/1e2, top=1.2*dmg['TDD'].max())
        
        
        "Solar cell degradation"
        funs.plot(plt.plot, [tcg]*2, [Psj*100, Psc*100], ['g', 'b'], ['-']*2, ['GaAs SJ', scname],
                  ['Coverglass thickness [$\mu$m]', 'Power ratio (EOL-to-BOL) [%]'], title='Solar cell degradation')
        plt.ylim(top=100.)
        
        
        "Single event upsets"
        funs.plot(plt.loglog, [tsh]*3, [(Ni + Np)/(DeltaT*365), Ni/(DeltaT*365), Np/(DeltaT*365)], ['k', 'r', 'b'], ['-']*3,
                  ['Total', 'Direct ionisation (ions)', 'Nuclear reactions (protons)'],
                  ['Al-equivalent shield thickness [mm] ', 'Average soft error rate [bit$^{-1}$ day$^{-1}$]'],
                  title='Soft errors in ' + dmgtarget + ' ' + devname + ' (' + shconf + ' shielding)')
        
        
        "Destructive single event effects"
        funs.plot(plt.loglog, [tsh], [Nd*100], ['k'], ['-'], [None],
          ['Al-equivalent shield thickness [mm] ', 'Probability of destructive SEE [%]'],
          title='Destructive SEEs in '+ dmgtarget + ' ' + devname + ' (' + shconf + ' shielding)')
        
        print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    
    
    """
    OUTPUT
    """
    if output:
        "Output directory"
        outpath = '../Output/' + infilename + '/'
        if not os.path.exists(outpath):
            os.mkdir(outpath)
        print('Saving results to "' + outpath + '"... ', end='')
        
        "Orbit"
        if trjtype != 'interplanetary' and len(t) > 1:
            pd.DataFrame({'Orbital time [s]': t, 'Altitude [km]': r[:,0]/1e3 - 6371., 'Latitude [deg]': 90 - np.degrees(r[:,1]),
                          'Longitude [deg]': np.degrees(r[:,2])}).to_csv(outpath + 'orb.csv', index=False)
        
        "Solar protons"
        pd.DataFrame({'Energy [MeV]': rad['SPR'].E, 'Differential fluence [cm-2 MeV-1]': rad['SPR'].dfluence,
                      'Mission-averaged exposure factor []': rad['SPR'].mfexp}).to_csv(outpath + 'spr.csv', index=False)
    
        "Trapped protons"
        pd.DataFrame({'Energy [MeV]': rad['TPR'].E,
                      'Differential fluence [cm-2 MeV-1]': rad['TPR'].dfluence}).to_csv(outpath + 'tpr.csv', index=False)
    
        "Trapped electrons"
        pd.DataFrame({'Energy [MeV]': rad['TEL'].E,
                      'Differential fluence [cm-2 MeV-1]': rad['TEL'].dfluence}).to_csv(outpath + 'tel.csv', index=False)
    
        "Galactic cosmic rays"
        gcrdflu_out = {'Energy [MeV/nucl]': rad['H gcr'].E, 'Differential fluence [cm-2 MeV-1]: H': rad['H gcr'].dfluence}
        gcrmfexp_out = {'Energy [MeV/nucl]': rad['H gcr'].E, 'Mission-averaged exposure factor []: H': rad['H gcr'].mfexp}
        for key in rad.keys():
            if 'gcr' in key and key != 'H gcr':
                gcrdflu_out[key[:2].strip()] = rad[key].dfluence
                gcrmfexp_out[key[:2].strip()] = rad[key].mfexp
        pd.DataFrame(gcrdflu_out).to_csv(outpath + 'gcr_dflu.csv', index=False)
        pd.DataFrame(gcrmfexp_out).to_csv(outpath + 'gcr_mfexp.csv', index=False)
        
        "Ionising dose"
        pd.DataFrame({'Al-equivalent ' + shconf + ' shield thickness [mm]': tsh, 'Ionising dose in ' + dmgtarget + ' [Gy]: Total': TID,
                      'Trapped electrons': rad['TEL'].idose, 'Bremsstrahlung': rad['TEL'].idoseBR, 'Trapped protons': rad['TPR'].idose,
                      'Solar protons': rad['SPR'].idose}).to_csv(outpath + 'id.csv', index=False)
        
        "Displacement dose"
        pd.DataFrame({'Al-equivalent ' + shconf + ' shield thickness [mm]': tsh, 'Displacement dose in ' + dmgtarget + ' [MeV g-1]: Total': TDD,
                      'Trapped electrons': rad['TEL'].ddose, 'Trapped protons': rad['TPR'].ddose,
                      'Solar protons': rad['SPR'].ddose}).to_csv(outpath + 'dd.csv', index=False)
        
        "Solar cell degradation"
        pd.DataFrame({'Coverglass (SiO2) thickness [um]': tcg, 'Power ratio (EOL-to-BOL) [%]: GaAs SJ': Psj*100,
                      scname: Psc*100}).to_csv(outpath + 'scdegr.csv', index=False)
    
        "Single event effects"
        pd.DataFrame({'Al-equivalent ' + shconf + ' shield thickness [mm]': tsh,
                      'Soft error rate [bit-1 day-1] in ' + dmgtarget + ' ' + devname + ': Total': (Ni + Np)/(DeltaT*365),
                      'Ions': Ni/(DeltaT*365), 'Protons': Np/(DeltaT*365),
                      'Probability of destructive SEE [%]': Nd*100}).to_csv(outpath + 'see.csv', index=False)
        
        print('Done. Run time: ' + str(round(time.time() - START, 2)) + ' s')
    
    print('MISSION END')
    
    return T, t, r, rad, dmg

######################################################################################################################################################