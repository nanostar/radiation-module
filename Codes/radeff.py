"""
Radiation effects module. It includes the necessary functions to compute the radiation effects of all radiation particles: ionising dose, displacement
dose, solar cell degradation and single event effects.
"""

import subprocess
import numpy as np
import transp
import shielding
import otherfuns as funs

######################################################################################################################################################

def IonisingDose(DeltaT, target, tAl, shconf, SPR, TPR, TEL):
    """
    Ionising dose function. Returns the ionising dose of solar protons, trapped protons, trapped electrons and secondary electrons (bremsstrahlung) in
    function of Al-equivalent planar or spherical shielding thickness using the SHIELDOSE-2 code.
    Input:
        - DeltaT: mission duration [yr] (float)
        - target: target material (str) (Al, graphite, Si, air, bone, CaF2, GaAs, LiF, SiO2, tissue, H2O)
        - tAl:    Al-equivalent shielding thickness [mm] (1D array of floats)
        - shconf: shielding configuration (planar or spherical) (str)
        - SPR:    solar proton radiation (RadPart object)
        - TPR:    trapped proton radiation (RadPart object)
        - TEL:    trapped electron radiation (RadPart object)
    Output:
        - Solar proton, trapped proton, trapped electron and bremmstrahlung ionising doses [Gy] for the given shielding configuration
          (1D arrays of floats [tAl]) (as attributes 'idose' of the corresponding RadPart objects) ('idoseBR' for bremsstrahlung, in
          trapped electron RadPart object)
        - TID:    total ionising dose [Gy] for the given shielding configuration (1D array of floats [tAl])
    """
    detmat = ['', 'Al', 'graphite', 'Si', 'air', 'bone', 'CaF2', 'GaAs', 'LiF', 'SiO2', 'tissue', 'H2O']
    
    "Input"
    IDET = str(detmat.index(target))
    INUC, IMAX, IUNT = '2', str(len(tAl)), '3'
    Z = ''.join([str(round(tAl[i], 6)) + ',' for i in range(len(tAl))])[:-1]
    
    "Solar protons"
    if np.all(SPR.dfluence == 0.):
        EMINS, EMAXS, NPTSP = str(0.1), str(10000.0), str(1001)
        JSMAX = str(0)
        EPSS, SS = '', ''
    else:
        sprE0, sprdflu0 = SPR.E[SPR.dfluence > 0.], SPR.dfluence[SPR.dfluence > 0.]
        sprE = np.logspace(np.log10(sprE0[0]), np.log10(sprE0[-1]), 90) if len(sprE0) > 90 else sprE0
        sprdflu = funs.loginterp(sprE0, sprdflu0, sprE) if len(sprE0) > 90 else sprdflu0
        EMINS, EMAXS, NPTSP = str(round(sprE.min(), 3)), str(round(sprE.max(), 3)), str(1001)
        JSMAX = str(len(sprE))
        EPSS = ''.join(['{:.4f}'.format(sprE[i]) + ',' for i in range(len(sprE))])[:-1] + '\n'
        SS = ''.join(['{:.4e}'.format(sprdflu[i]) + ',' for i in range(len(sprdflu))])[:-1] + '\n'
    
    "Trapped protons"
    if np.all(TPR.mdflux == 0.):
        EMINP, EMAXP = str(0.1), str(10000.0)
        JPMAX = str(0)
        EPSP, SP = '', ''
    else:
        tprE0, tprdflx0 = TPR.E[TPR.mdflux > 0.], TPR.mdflux[TPR.mdflux > 0.]
        tprE = np.logspace(np.log10(tprE0[0]), np.log10(tprE0[-1]), 90) if len(tprE0) > 90 else tprE0
        tprdflx = funs.loginterp(tprE0, tprdflx0, tprE) if len(tprE0) > 90 else tprdflx0
        EMINP, EMAXP = str(round(tprE.min(), 3)), str(round(tprE.max(), 3))
        JPMAX = str(len(tprE))
        EPSP = ''.join(['{:.4f}'.format(tprE[i]) + ',' for i in range(len(tprE))])[:-1] + '\n'
        SP = ''.join(['{:.4e}'.format(tprdflx[i]) + ',' for i in range(len(tprdflx))])[:-1] + '\n'
    
    "Trapped electrons"
    if np.all(TEL.mdflux == 0.):
        EMINE, EMAXE, NPTSE = str(0.05), str(10.0), str(1001)
        JEMAX = str(0)
        EPSE, SE = '', ''
    else:
        telE0, teldflx0 = TEL.E[TEL.mdflux > 0.], TEL.mdflux[TEL.mdflux > 0.]
        telE = np.logspace(np.log10(telE0[0]), np.log10(telE0[-1]), 90) if len(telE0) > 90 else telE0
        teldflx = funs.loginterp(telE0, teldflx0, telE) if len(telE0) > 90 else teldflx0
        EMINE, EMAXE, NPTSE = str(round(telE.min(), 3)), str(round(telE.max(), 3)), str(1001)
        JEMAX = str(len(telE))
        EPSE = ''.join(['{:.4f}'.format(telE[i]) + ',' for i in range(len(telE))])[:-1] + '\n'
        SE = ''.join(['{:.4e}'.format(teldflx[i]) + ',' for i in range(len(teldflx))])[:-1] + '\n'
    
    COMMENT = 'NO COMMENT'
    EUNIT, TINTER = str(1.0), '{:.5e}'.format(DeltaT*365*24*3600)
    
    "Input file"
    infile = open('./SHIELDOSE2/sd2in.INP', 'w')
    infile.write('sd2out.OUT\n')
    infile.write('sd2out.ARR\n')
    infile.write(IDET + ',' + INUC + ',' + IMAX + ',' + IUNT + '\n')
    infile.write(Z + '\n')
    infile.write(EMINS + ',' + EMAXS + ',' + EMINP + ',' + EMAXP + ',' + NPTSP + ',' + EMINE + ',' + EMAXE + ',' + NPTSE + '\n')
    infile.write(COMMENT + '\n')
    infile.write(JSMAX + ',' + JPMAX + ',' + JEMAX + ',' + EUNIT + ',' + TINTER + '\n')
    infile.write(EPSS)
    infile.write(SS)
    infile.write(EPSP)
    infile.write(SP)
    infile.write(EPSE)
    infile.write(SE)
    infile.close()
    
    "Run SHIELDOSE-2"
    subprocess.call('cmd /c "cd .\SHIELDOSE2 & (echo sd2in.INP | shieldose2.exe)"')
    
    "Output file"
    outfile = open('./SHIELDOSE2/sd2out.OUT', 'r')
    rows = outfile.readlines()
    outfile.close()
    
    "Output reading"
    i0 = [i + 2 for i, e in enumerate(rows) if e ==
          '    Z(mils)      Z(mm)   Z(g/cm2)   ELECTRON    BREMS      EL+BR     TRP PROT   SOL PROT  EL+BR+TRP    TOTAL\n']
    ID_SPR, ID_TPR, ID_TE, ID_BR = np.zeros((len(tAl), 3)), np.zeros((len(tAl), 3)), np.zeros((len(tAl), 3)), np.zeros((len(tAl), 3))
    for j in range(3):
        for k in range(len(tAl)):
            values = [float(s) for s in rows[i0[j]+k].split()]
            f = 2 if j == 2 else 1 # SHIELDOSE-2 returns 1/2 the dose for sphere geometry
            ID_SPR[k,j] = values[7]*f/100 # [rad] to [Gy]
            ID_TPR[k,j] = values[6]*f/100
            ID_TE[k,j], ID_BR[k,j] = values[3]*f/100, values[4]*f/100
    
    "Total ionising dose"
    TID = ID_SPR + ID_TPR + ID_TE + ID_BR
    
    "Output assignment"
    c = 1 if shconf == 'planar' else 2
    setattr(SPR, 'idose', ID_SPR[:,c])
    setattr(TPR, 'idose', ID_TPR[:,c])
    setattr(TEL, 'idose', ID_TE[:,c])
    setattr(TEL, 'idoseBR', ID_BR[:,c])
    
    return TID[:,c]

######################################################################################################################################################

def DisplacementDose(tAl, SPR, TPR, TEL):
    """
    Displacement (non-ionising) dose function. Returns the displacement dose of solar protons, trapped protons and trapped electrons in function of
    Al-equivalent shielding using the screened-relativistic non-ionising energy loss (SR-NIEL) approach.
    Input:
        - tAl:    Al-equivalent shielding thickness [mm] (1D array of floats)
        - SPR:    solar proton radiation (RadPart object)
        - TPR:    trapped proton radiation (RadPart object)
        - TEL:    trapped electron radiation (RadPart object)
    Output:
        - Solar proton, trapped proton and trapped electron displacement doses [MeV g-1] for the given shielding (1D arrays of floats [tAl])
          (as attributes 'ddose' of the corresponding RadPart objects)
        - TDD:    total displacement dose [MeV g-1] for the given shielding (1D array of floats [tAl])
    """
    "Solar protons"
    DD_SPR = np.zeros(len(tAl)) if np.all(SPR.dfluence == 0.) else np.trapz(SPR.NIEL*SPR.dfluencet, SPR.E, axis=1)
    setattr(SPR, 'ddose', DD_SPR)
    
    "Trapped protons"
    DD_TPR = np.zeros(len(tAl)) if np.all(TPR.dfluence == 0.) else np.trapz(TPR.NIEL*TPR.dfluencet, TPR.E, axis=1)
    setattr(TPR, 'ddose', DD_TPR)
    
    "Trapped electrons"
    DD_TEL = np.zeros(len(tAl)) if np.all(TEL.dfluence == 0.) else np.trapz(TEL.NIEL*TEL.dfluencet, TEL.E, axis=1)
    setattr(TEL, 'ddose', DD_TEL)
    
    "Total displacement dose"
    TDD = DD_SPR + DD_TPR + DD_TEL
    
    return TDD

######################################################################################################################################################

def SolarCellDegradation(tcg, SPR, TPR, TEL):
    """
    Solar cell degradation function. Returns the function returning the degradation caused by the displacement damage of solar protons, trapped
    protons and trapped electrons using the displacement damage dose (DDD) approach. The material of the solar cell is treated as GaAs for particle
    transport purposes.
    Input:
        - tcg:       coverglass (SiO2) thickness [um] (1D array of floats)
        - SPR:       solar proton radiation (RadPart object)
        - TPR:       trapped proton radiation (RadPart object)
        - TEL:       trapped electron radiation (RadPart object)
    Output:
        - scdegrfun: solar cell power degradation function (function(degrdata))
    """
    def scdegrfun(Cp, Dxp, Ce, Dxe, n):
        "1 MeV electron NIEL"
        NIEL1MeVel = float(transp.niel(-1, np.array([1.]), 'GaAs'))
        
        
        "SOLAR PROTONS"
        if np.all(SPR.dfluence == 0.):
            DD_SPR = np.zeros(tcg)
        else:
            "Transmitted (shielded) fluence"
            tAlSPR = tcg/1e4*2.33*np.trapz(SPR.RpAl/SPR.Rpcg, SPR.E)/(SPR.E[-1] - SPR.E[0])
            SPRdflt = shielding.AlEqShielding(SPR.Z, SPR.A, SPR.E, SPR.dfluence, SPR.SAl, SPR.RpAl, tAlSPR, 'planar')
            setattr(SPR, 'dfluencesc', SPRdflt)
            
            "Displacement dose"
            DD_SPR = np.trapz(SPR.NIELsc*SPRdflt, SPR.E, axis=1)
            setattr(SPR, 'ddosesc', DD_SPR)
        
        
        "TRAPPED PROTONS"
        if np.all(TPR.dfluence == 0.):
            DD_TPR = np.zeros(DD_SPR.shape)
        else:
            "Transmitted (shielded) fluence"
            tAlTPR = tcg/1e4*2.33*np.trapz(TPR.RpAl/TPR.Rpcg, TPR.E)/(TPR.E[-1] - TPR.E[0])
            TPRdflt = shielding.AlEqShielding(TPR.Z, TPR.A, TPR.E, TPR.dfluence, TPR.SAl, TPR.RpAl, tAlTPR, 'planar')
            setattr(TPR, 'dfluencesc', TPRdflt)
            
            "Displacement dose"
            DD_TPR = np.trapz(TPR.NIELsc*TPRdflt, TPR.E, axis=1)
            setattr(TPR, 'ddosesc', DD_TPR)
            
        
        
        "TRAPPED ELECTRONS"
        if np.all(TEL.dfluence == 0.):
            DD_TEL = np.zeros(DD_SPR.shape)
        else:
            "Transmitted (shielded) fluence"
            tAlTEL = tcg/1e4*2.33*np.trapz(TEL.RpAl/TEL.Rpcg, TEL.E)/(TEL.E[-1] - TEL.E[0])
            TELdflt = shielding.AlEqShielding(TEL.Z, TEL.A, TEL.E, TEL.dfluence, TEL.SAl, TEL.RpAl, tAlTEL, 'planar')
            setattr(TEL, 'dfluencesc', TELdflt)
            
            "Displacement dose"
            DD_TEL = np.trapz(TEL.NIELsc**n*TELdflt, TEL.E, axis=1)/NIEL1MeVel**(n - 1)
            setattr(TEL, 'ddosesc', DD_TEL)
        
        "Convert 1MeV-equivalent electron DDD to proton DDD"
        Dde = DD_TEL
        Ddep = Dxp*((1 + Dde/Dxe)**(Ce/Cp) - 1)
        
        "Total proton-equivalent DDD"
        Ddp = DD_SPR + DD_TPR + Ddep
        
        "Maximum power degradation"
        Pmp = 1 - Cp*np.log10(1 + Ddp/Dxp)
        
        return Pmp
    
    "Solar cell degradation function"
    return scdegrfun

######################################################################################################################################################

def SingleEventEffects(radparts, tAl, devdata):
    """
    Single event effects function. Returns the single event upsets and destructive effects of the given device for the given radiation environment and
    shielding.
    Input:
        - radparts: radiation particles (dictionary of RadPart objects)
        - tAl:      Al-equivalent shielding thickness [mm] (1D array of floats)
        - devdata:  device data (dims, L0, L0d, pfitp) (dimensions (length, width, height) [um] (tuple of floats), upset LET threshold [MeV cm2 g-1]
                    (float), destructive LET threshold [MeV cm2 g-1] (float), proton cross section Bendel fit parameters (A [MeV], sigmasatp
                    [cm2 bit-1]) (tuple of floats))
    Output:
        - Ni:       number of single event upsets caused by direct ionisation [bit-1]
        - Np:       number of single event upsets caused by proton-induced nuclear reactions [bit-1]
        - Nd:       number of destructive single event effects [bit-1]
    """
    "DEVICE DATA"
    "Dimensions"
    l, w, h = devdata[0]
    Adev = 2*(l*w + l*h + h*w)
    
    "Direct ionisation upset and destructive LET thresholds"
    L0, L0d = devdata[1], devdata[2]
    
    "Nuclear reactions cross section (protons)"
    A, sigmasatp = devdata[3]
    Y = lambda E: (18/A)**0.5*(E - A)
    sigmap = lambda E: sigmasatp*(1 - np.exp(-0.18*Y(E)**0.5))**4
    
    
    "DIRECT IONISATION SEUs"
    "LET spectra"
    LETmin = min(radparts['SPR'].LET.min(), np.inf if np.all(radparts['TPR'].dfluence == 0.) else radparts['TPR'].LET.min(),
                 radparts['H gcr'].LET.min())
    LETmax = radparts['U gcr'].LET.max()
    LET = np.logspace(np.log10(LETmin), np.log10(LETmax), 100)
    dflu_dLET = np.zeros((len(tAl), len(LET)))
    for part in radparts.values():
        "Total SEUs (fluences) NO TENGO MODELO DE FLUENCIA DE PARTÍCULAS SOLARES NO TENGO MODELO DE FLUENCIA DE PARTÍCULAS SOLARES"
        if hasattr(part, 'dfluencet') and hasattr(part, 'LET') and part.Z > -1:
            "Transmitted (shielded) fluence differential LET spectra"
            dF_dL = part.dfluencet*abs(np.gradient(part.E, part.LET))
            dflu_dLET += funs.loginterp(part.LET, dF_dL, LET, fill=np.nan)/(4*np.pi) # [cm-2 sr-1 (MeV cm2 g-1)-1]
    
    "Integral chord-length distribution function"
    CLfun = IntegralChordLengthDistributionRPP((l, w, h))
    
    "Upset number and maximum rate"
    Ni = Adev/1e8/4*np.trapz(dflu_dLET*CLfun(h*L0/LET), LET, axis=1) # [bit-1]
    
    "Destructive effects number"
    idxd = np.where(LET >= L0d)[0][0]
    Nd = Adev/1e8/4*np.trapz(dflu_dLET[:,idxd:]*CLfun(h*L0d/LET[idxd:]), LET[idxd:], axis=1) # [bit-1]
    
    
    "PROTON SEUs"
    "Energy spectra"
    Ep = np.logspace(np.log10(A), np.log10(radparts['H gcr'].E.max()), 100)
    dflu_dE = np.zeros((len(tAl), len(Ep)))
    for part in radparts.values():
        "Total SEUs (fluences)"
        if hasattr(part, 'dfluencet') and part.Z == 1:
            "Transmitted (shielded) fluence differential E spectra"
            dflu_dE += funs.loginterp(part.E, part.dfluencet, Ep, fill=np.nan) # [cm-2 MeV-1]
            
    "Number of upsets"
    Np = np.trapz(dflu_dE*sigmap(Ep), Ep, axis=1) # [bit-1]
    
    
    return Ni, Np, Nd

######################################################################################################################################################

def IntegralChordLengthDistributionRPP(dims):
    """
    Rectangular parallelepiped integral chord-length distribution function. Returns the integral chord-length distribution function of a rectangular
    parallelepiped with the given dimensions
    Input:
        - dims: device dimensions (length, width, height) [arbitrary units] (tuple of floats)
    Output:
        - C:    C(>x) integral chord-length distribution function [arbitrary units] (function)
    """
    "Geometrical parameters"
    c, b, a = dims
    V, S = a*b*c, 2*(a*b + b*c + a*c)
    y, z, r, w = 6*np.pi*V, a + b + c, a**2*b**2 + b**2*c**2 + a**2*c**2, (a**2 + b**2 + c**2)**0.5
    vc, vb, va = (a**2 + b**2)**0.5, (a**2 + c**2)**0.5, (b**2 + c**2)**0.5
    s = lambda x, gamma: abs(x**2 - gamma**2)**0.5
    G = lambda x, gamma: 8*s(x, gamma) - 2*gamma**2*s(x,gamma)/x**2 - 6*gamma*np.arctan(s(x, gamma)/gamma)
    
    T = lambda x, x1, x2: (12*V*((-x1/(2*x**2) + 1/(2*x1))*np.arctan(abs(x**2 - (x1**2 + x2**2))**0.5/x2) + (-x2/(2*x**2) + 1/(2*x2))*
                                 np.arctan(abs(x**2 - (x1**2 + x2**2))**0.5/x1) - 1/(2*(x1**2 + x2**2)**0.5)*(x2/x1 + x1/x2)*np.arctan(abs(x**2 -
                                          (x1**2 + x2**2))**0.5/(x1**2 + x2**2)**0.5)))
    
    "Chord-length subfunctions"
    f0 = lambda x: -9*x**2/2 + 8*z*x
    fa = lambda x: (a*y - a**4)/2*(1/a**2 - 1/x**2) + 8*(b + c)*(x - a) - (b + c)*G(x,a)
    fb = lambda x: ((a + b)*y - (a**4 + b**4))/2*(1/b**2 - 1/x**2) + 9*(x**2 - b**2)/2 + 8*c*(x - b) - (b + c)*(G(x,a) - G(b,a)) - (c + a)*G(x,b)
    fvc = lambda x: (((a + b)*y - 6*a**2*b**2)/2*(1/vc**2 - 1/x**2) + 8*c*(x - vc) + c*G(x,vc) - c*(G(x,a) - G(vc,a)) - c*(G(x,b) -
                      G(vc,b)) - T(x,a,b))
    fc = lambda x: ((z*y - (a**4 + b**4 + c**4))/2*(1/c**2 - 1/x**2) + 9*(x**2 - c**2) - (b + c)*(G(x,a) - G(c,a)) - (c + a)*(G(x,b) - G(c,b)) -
                    (a + b)*G(x,c))
    x0 = max(c, vc)
    fx0 = lambda x: ((z*y - c**4 - 6*a**2*b**2)/2*(1/x0**2 - 1/x**2) + 9*(x**2 - x0**2)/2 + c*(G(x,vc) - G(x0,vc)) - c*(G(x,a) - G(x0,a)) -
                    c*(G(x,b) - G(x0,b)) - (a + b)*(G(x,c) - G(x0,c)) - (T(x,a,b) - T(x0,a,b)))
    fvb = lambda x: ((z*y + a**4 - 6*a**2*va**2)/2*(1/vb**2 - 1/x**2) + b*G(x,vb) + c*(G(x,vc) - G(vb,vc)) - b*(G(x,c) - G(vb,c)) -
                     c*(G(x,b) - G(vb,b)) - (T(x,a,b) - T(vb,a,b)) - T(x,a,c))
    fva = lambda x: ((z*y + w**4 - 8*r)/2*(1/va**2 - 1/x**2) + 9*(x**2 - va**2)/2 + a*G(x,va) + b*(G(x,vb) - G(va,vb)) +
                     c*(G(x,vc) - G(va,vc)) - T(x,b,c) - (T(x,c,a) - T(va,c,a)) - (T(x,a,b) - T(va,a,b)))
    
    "Integral chord-length functions"
    C1 = lambda x: 1 - 2/(3*np.pi*S)*f0(x)
    C2 = lambda x: C1(a) - 2/(3*np.pi*S)*fa(x)
    C3 = lambda x: C2(b) - 2/(3*np.pi*S)*fb(x)
    C4 = lambda x: C3(vc) - 2/(3*np.pi*S)*fvc(x) if vc < c else C3(c) - 2/(3*np.pi*S)*fc(x)
    C5 = lambda x: C4(c) - 2/(3*np.pi*S)*fx0(x) if vc < c else C4(vc) - 2/(3*np.pi*S)*fx0(x)
    C6 = lambda x: C5(vb) - 2/(3*np.pi*S)*fvb(x)
    C7 = lambda x: C6(va) - 2/(3*np.pi*S)*fva(x)
    
    "Integral chord-length distribution function"
    def clfun(x):
        C = np.zeros(len(x))
        for i in range(len(x)):
            if 0 < x[i] <= a:
                C[i] = C1(x[i])
            elif a < x[i] <= b:
                C[i] = C2(x[i])
            elif b < x[i] <= min(c, vc):
                C[i] = C3(x[i])
            elif min(c, vc) < x[i] <= max(c, vc):
                C[i] = C4(x[i])
            elif max(c, vc) < x[i] <= vb:
                C[i] = C5(x[i])
            elif vb < x[i] <= va:
                C[i] = C6(x[i])
            elif va < x[i] <= w:
                C[i] = C7(x[i])
            else:
                C[i] = 0.
        return C
    
    return clfun

######################################################################################################################################################