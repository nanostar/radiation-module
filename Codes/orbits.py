"""
Orbits module. It includes the functions to generate mission trajectories (GEO, general Earth, interplanetary and custom orbits).
"""

import numpy as np
from scipy import optimize
import coordtransf

######################################################################################################################################################

def GEO(phi):
    """
    Geostationary orbit function. Returns a geostationary orbit in spherical geographic coordinates. This orbit is treated as a single point since all
    corresponding fluxes are orbit- or mission-averaged.
    Input:
        - phi: longitude [deg] (float)
    Output:
        - tau: orbital period [s] (float)
        - t:   orbital time [s] (1D array of float)
        - r:   position vector [spherical geographic] [m, rad, rad] (2D array of floats: [t, [r, theta, phi]])
    """
    "Orbital period"
    tau = 23*3600 + 56*60 + 4
    
    "Orbital time"
    t = np.array([0.])
    
    "Orbital position"
    r = np.array([42164e3, np.pi/2, phi*np.pi/180])*np.ones((len(t),3))
    
    return tau, t, r

######################################################################################################################################################

def general(elems, Norb, orbres):
    """
    General Earth orbit function. Returns an Earth orbit with the given orbital elements in spherical geographic coordinates.
    Input:
        - elems:  orbital elements [km, deg] (['ae', a, e, i, Omega, omega] or ['h', hp, ha, i, Omega, omega]) (list)
        - Norb:   number of orbits (int)
        - orbres: orbital resolution (in true anomaly) (int)
    Output:
        - tau:    orbital period [s] (float)
        - t:      orbital time [s] (1D array of floats)
        - r:      position vector [spherical geographic] [m, rad, rad] (2D array of floats: [t, [r, theta, phi]])
    """
    "Constants"
    RE = 6371. # [km]
    muE = 3.986e5 # [km3 s-2]
    
    "Orbital elements"
    if elems[0] == 'ae':
        a, e, i, Omega, omega = elems[1:] # [km, deg]
    elif elems[0] == 'h':
        hp, ha, i, Omega, omega = elems[1:] # [km, deg]
        a, e = RE + (hp + ha)/2, (ha - hp)/(2*RE + hp + ha)
    
    "Orbital period"
    tau = 2*np.pi*np.sqrt(a**3/muE)
    
    "Orbital time"
    nu = np.radians(np.tile(np.linspace(0., 360., orbres), Norb))
    k = np.floor(np.arange(len(nu))/orbres)
    E = 2*np.arctan(np.sqrt((1 - e)/(1 + e))*np.tan(nu/2))
    E[E < 0.] += 2*np.pi
    t = np.sqrt(a**3/muE)*(2*np.pi*k + E - e*np.sin(E))
    
    "Orbital position (Kepler problem)"
    r = np.zeros((len(t), 3))
    for j in range(len(t)):
        r0 = a*1e3*(1 - e**2)/(1 + e*np.cos(nu[j]))
        phiG = 2*np.pi*t[j]/(23*3600 + 56*60 + 4)
        r[j] = coordtransf.cart2spher(coordtransf.GEItoGEO(coordtransf.ORBtoGEI(r0*np.array([np.cos(nu[j]), np.sin(nu[j]), 0.]),
         i*np.pi/180, Omega*np.pi/180, omega*np.pi/180), phiG))
    
    return tau, t, r

######################################################################################################################################################

def interplanetary(elems, T):
    """
    General interplanetary heliocentric orbit function. Returns the mission-averaged heliocentric distance for the orbit with the given heliocentric 
    orbital elements.
    Input:
        - elems:  orbital elements (heliocentric) [AU, deg] (['ae', a, e, i, Omega, omega, nu0] or ['r', rp, ra, i, Omega, omega, nu0]) (list)
        - T:      mission epochs [yr] (1D array of floats)
    Output:
        - rhmean: mission-averaged heliocentric distance [AU] (float)
    """
    "Constants"
    AU = 1.49598e8 # [km]
    muS = 1.327e11 # [km3 s-2]
    
    "Orbital elements"
    if elems[0] == 'ae':
        a, e, i, Omega, omega, nu0 = elems[1:] # [AU, deg]
    elif elems[0] == 'r':
        rp, ra, i, Omega, omega, nu0 = elems[1:] # [AU, deg]
        a, e = (rp + ra)/2, (ra - rp)/(rp + ra)
    nu0 *= np.pi/180 # [rad]
    
    "Orbital period"
    tau = 2*np.pi*np.sqrt((a*AU)**3/muS)
    
    "Orbital time"
    t = np.linspace(0., (T[-1] - T[0])*(365*24*3600), 1000)
    
    "Orbital position (inverse Kepler problem)"
    n = np.sqrt(muS/(a*AU)**3)
    k = np.floor(t/tau)
    E0 = 2*np.arctan(np.sqrt((1 - e)/(1 + e))*np.tan(nu0/2))
    M0 = E0 - e*np.sin(E0)
    M = M0 + n*t - 2*k*np.pi
    E = optimize.newton(lambda x: x - e*np.sin(x) - M, M)
    nu = 2*np.arctan(np.sqrt((1 + e)/(1 - e))*np.tan(E/2))
    nu[nu < 0.] += 2*np.pi
    rh = a*(1 - e**2)/(1 + e*np.cos(nu))
    
    "Mission-averaged heliocentric distance"
    rhmean = np.trapz(rh, t)/t[-1]
    
    return rhmean

######################################################################################################################################################
    
def custom(TLE, Norb, orbres):
    """
    General custom orbit function. Returns an Earth orbit with the orbital elements in the given TLE (Two-Line Element set) in spherical geographic
    coordinates.
    Input:
        - TLE:    name of input TLE file (str) (must be in Trajectories folder)
        - Norb:   number of orbits (int)
        - orbres: orbital resolution (in true anomaly) (int)
    Output:
        - tau:    orbital period [s] (float)
        - t:      orbital time [s] (1D array of floats)
        - r:      position vector [spherical geographic] [m, rad, rad] (2D array of floats: [t, [r, theta, phi]])
    """
    "Constants"
    muE = 3.986e5 # [km3 s-2]
    
    "Read TLE"
    tlefile = open('../Data/Trajectories/' + TLE + '.txt', 'r')
    tledata = tlefile.readlines()[1].split()
    tlefile.close()
    
    "Orbital elements"
    tau = 24*3600/float(tledata[7])
    a = ((tau/(2*np.pi))**2*muE)**(1/3)
    e = float('0.' + tledata[4])
    i, Omega, omega = (float(tledata[k]) for k in [2, 3, 5])
    
    "Orbital time"
    nu = np.radians(np.tile(np.linspace(0., 360., orbres), Norb))
    k = np.floor(np.arange(len(nu))/orbres)
    E = 2*np.arctan(np.sqrt((1 - e)/(1 + e))*np.tan(nu/2))
    E[E < 0.] += 2*np.pi
    t = np.sqrt(a**3/muE)*(2*np.pi*k + E - e*np.sin(E))
    
    "Orbital position"
    r = np.zeros((len(t), 3))
    for j in range(len(t)):
        r0 = a*1e3*(1 - e**2)/(1 + e*np.cos(nu[j]))
        phiG = 2*np.pi*t[j]/(23*3600 + 56*60 + 4)
        r[j] = coordtransf.cart2spher(coordtransf.GEItoGEO(coordtransf.ORBtoGEI(r0*np.array([np.cos(nu[j]), np.sin(nu[j]), 0.]),
         i*np.pi/180, Omega*np.pi/180, omega*np.pi/180), phiG))
    
    return tau, t, r

######################################################################################################################################################