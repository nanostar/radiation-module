"""
Particle transport module. It includes the necessary functions to compute the transport properties of radiation particles in the given shielding
and target (for damage assessment) materials: stopping power, range and non-ionising energy loss.
"""

import subprocess
import math
import numpy as np
import pandas as pd
import otherfuns as funs

######################################################################################################################################################

def material(target):
    """
    Target material function. Returns material data of the given target.
    Input:
        - target: target material (Al, SiO2, Si or GaAs) (str)
    Output:
        - Zt:     atomic number of the elements in the target material (list of ints)
        - Nt:     stoichiometric coefficients of the elements in the target material (list of ints)
        - At:     mass numbers of the elements in the target material [u] (list of floats)
        - rhot:   density of the target material [g cm-3] (float)
    """
    "Material data"
    Zt = {'Al': [13], 'SiO2': [14, 8], 'Si': [14], 'GaAs': [31, 33]}
    Nt = {'Al': [1], 'SiO2': [1, 2], 'Si': [1], 'GaAs': [1, 1]}
    At = {'Al': [26.98], 'SiO2': [28.09, 16.], 'Si': [28.09], 'GaAs': [69.72, 74.92]}
    rhot = {'Al': 2.70, 'SiO2': 2.33, 'Si': 2.33, 'GaAs': 5.31}
    
    return Zt[target], Nt[target], At[target], rhot[target]

######################################################################################################################################################

def niel(Zi, Ei, target):
    """
    Non-ionising energy loss (NIEL) function. Returns the NIEL of the given incident particle in the given target material in function of energy. Data
    available for protons and electrons in Si and GaAs [Baur et al 2014, SPENVIS, SR-NIEL].
    Input:
        - Zi:     atomic number of incident particle (proton: 1 or electron: -1) (int)
        - Ei:     incident particle kinetic energy [MeV] (1D array of floats)
        - target: target material (Si or GaAs) (str)
    Output:
        - NIEL:   non-ionising energy loss of the incident particle in the target material [MeV cm2 g-1] (1D array of floats [E])
    """
    "Incident particle"
    if Zi == 1:
        incpart = 'p'
    elif Zi == -1:
        incpart = 'e'
    
    "NIEL data"
    data = pd.read_csv("../Data/Stopping and transport/NIEL/" + incpart + "_" + target +".csv", header=1)
    Edata, NIELdata = data.values[:,0], data.values[:,1]
    
    "NIEL interpolation"
    NIEL = funs.loginterp(Edata, NIELdata, Ei)
    NIEL[Ei < Edata[0]] = 0.
    
    return NIEL

######################################################################################################################################################

def stopping(Zi, Ai, Ei, target):
    """
    Stopping function. Returns the stopping power and range of the given particle in the given material in function of kinetic energy using:
        - Electrons:        ESTAR database [NIST] with projected range correction [Tabata et al.]
        - Protons and ions: SRIM code (SR module)
    Input:
        - Zi:     atomic number of the incident particle (int)
        - Ai:     mass number of the incident particle [u] (float)
        - Ei:     kinetic energy of the incident particle [MeV/u] (1D array of floats)
        - target: target material (Al, SiO2, Si or GaAs) (str)
    Output:
        - ESP:    electronic (inelastic/collision) stopping power of the incident particle in the target material [MeV cm2 g-1]
                  (1D array of floats [E])
        - NRSP:   nuclear (elastic) (protons and ions) or radiative (electrons) stopping power of the incident particle in the target material
                  [MeV cm2 g-1] (1D array of floats [E])
        - TSP:    total (electronic + nuclear for protons and ions, electronic + radiative for electrons) stopping power of the incident particle in
                  the target material [MeV cm2 g-1] (1D array of floats [E])
        - Rp:     projected range of the incident particle in the target material [g cm-2] (1D array of floats [E])
    """
    "Target material"
    Zt, Nt, At, rhot = material(target)
    
    # Electrons
    if Zi == -1:
        "Effective target atomic number"
        Zteff = np.sum(np.array(Zt)*np.array(Nt)*np.array(At))/np.sum(np.array(Nt)*np.array(At))
        
        "ESTAR data"
        edata = pd.read_csv("../Data/Stopping and transport/Stopping power and range/e_" + target +".csv", header=1)
        Edata, ESPdata, NRSPdata, RCSDAdata = edata.values[:,0], edata.values[:,1], edata.values[:,2], edata.values[:,4]
        
        "Projected range"
        tau = Edata/0.511 # E/(me c2) [MeV]
        a0 = np.array([[0.2946, 18.4, 6.59, 0.05242, 0.958, 0.2808],[0.274, -3.457, -2.414, 0.3709, -2.02, 0.2042],[0., 1.377, 1.094, 0., 1.099, 0.]])
        a = lambda i: a0[0,i]*Zteff**(a0[1,i] + a0[2,i]*math.log(Zteff))
        Rp_RCSDA = 1/(a(0) + a(1)/(1 + a(2)/tau**a(3) + a(4)*tau**a(5)))
        Rp_RCSDA[Rp_RCSDA > 1.0] = 1.0
        Rpdata = Rp_RCSDA*RCSDAdata
    
    # Protons and ions
    else:
        "Input file"
        infile = open('./SRIM/SR.IN', 'w')
        infile.write('---Stopping/Range Input Data (Number-format: Period = Decimal Point)\n')
        infile.write('---Output File Name\n')
        infile.write('"SRout.OUT"\n')
        infile.write('---Ion(Z), Ion Mass(u)\n')
        infile.write(str(Zi) + ',' + str(Ai) + '\n')
        infile.write('---Target Data: (Solid=0,Gas=1), Density(g/cm3), Compound Corr.\n')
        infile.write('0,' + str(rhot) + ',1.0\n')
        infile.write('---Number of Target Elements\n')
        infile.write(str(len(Zt)) + '\n')
        infile.write('---Target Elements: (Z), Target name, Stoich, Target Mass(u)\n')
        for i in range(len(Zt)):
            infile.write(str(Zt[i]) + ',"' + str(Zt[i]) + '",' + str(Nt[i]) + ',' + str(At[i]) + '\n')
        infile.write('---Output Stopping Units (1-8)\n')
        infile.write('6\n')
        infile.write('---Ion Energy : E-Min(keV), E-Max(keV)\n')
        infile.write('1.0E-01,5.0E+06')
        infile.close()
        
        "Run SRIM"
        subprocess.call('cmd /c "cd .\SRIM & SRModule.exe"')
        
        "Output file"
        outfile = open('./SRIM/SRout.OUT', 'r')
        rows = outfile.readlines()
        outfile.close()
        
        "Output reading"
        i0 = rows.index('-----------  ---------- ---------- ----------  ----------  ----------\n') + 1
        i1 = rows.index('-----------------------------------------------------------\n')
        Edata, ESPdata, NRSPdata, Rpdata = np.zeros(i1 - i0), np.zeros(i1 - i0), np.zeros(i1 - i0), np.zeros(i1 - i0)
        for i in range(i0, i1):
            contents = rows[i].split()
            Eunits, Efactor = ['eV', 'keV', 'MeV', 'GeV'], [1e-6, 1e-3, 1.0, 1e3]
            Edata[i-i0] = round(float(contents[0])*Efactor[Eunits.index(contents[1])], 6)
            ESPdata[i-i0] = float(contents[2])
            NRSPdata[i-i0] = float(contents[3])
            Runits, Rfactor = ['A', 'um', 'mm', 'm'], [1e-8, 1e-4, 1e-1, 1e2]
            Rpdata[i-i0] = float(contents[4])*Rfactor[Runits.index(contents[5])]*rhot
        
    "Stopping powers and projected range at input energies"
    ESP = funs.loginterp(Edata, ESPdata, Ei)
    NRSP = funs.loginterp(Edata, NRSPdata, Ei)
    TSP = funs.loginterp(Edata, ESPdata + NRSPdata, Ei)
    Rp = funs.loginterp(Edata, Rpdata, Ei)
    
    return ESP, NRSP, TSP, Rp

######################################################################################################################################################