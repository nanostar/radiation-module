"""
Radiation sources module. It includes the necessary functions to generate all radiation sources, their fluxes/fluences and properties.
"""

import math
import numpy as np
import pandas as pd
from scipy import special, interpolate
import solar
import geomag
import transp
import otherfuns as funs

######################################################################################################################################################

class RadPart(object):
    """
    Representation of a radiation particle.
    
    """
    def __init__(self, name, Z, A, m, E, niel=False, target=None, scdegr=False):
        """
        Creates a RadPart instance with attributes:
            - name:   name (str)
            - Z:      atomic number (int)
            - A:      mass number [u] (float)
            - m:      rest mass [MeV/c2] (float)
            - E:      particle energy [MeV/nucl] (1D array of floats)
            - P:      particle rigidity [MeV/e/c] (1D array of floats [E])
            - SAl:    total stopping power [MeV cm2 g-1] in aluminium (1D array of floats [E])
            - RpAl:   projected range [g cm-2] in aluminium (1D array of floats [E])
            - LET:    linear energy transfer (electronic stopping power) [MeV cm2 g-1] in target material (Si or GaAs) (1D array of floats [E])
            - NIEL:   non-ionising energy loss [MeV cm2 g-1] in target material (Si or GaAs) (1D array of floats [E])
            - Scg:    total stopping power [MeV cm2 g-1] in solar cell coverglass (SiO2) (1D array of floats [E])
            - Rpcg:   projected range [g cm-2] in solar cell coverglass (SiO2) (1D array of floats [E])
            - NIELsc: non-ionising energy loss [MeV cm2 g-1] in solar cell material (GaAs) (1D array of floats [E])
        """
        self.name = name
        
        "Particle properties"
        self.Z = Z
        self.A = A
        self.m = m
        self.E = E
        self.P = A/Z*np.sqrt(E*(E + 2*m))
        
        "Transport properties"
        _, _, self.SAl, self.RpAl = transp.stopping(Z, A, E, 'Al')
        if target is not None:
            if Z > -1:
                self.LET, _, _, _ = transp.stopping(Z, A, E, target)
            if niel is True:
                self.NIEL = transp.niel(Z, E, target)
        if scdegr is True:
            _, _, self.Scg, self.Rpcg = transp.stopping(Z, A, E, 'SiO2')
            self.NIELsc = transp.niel(Z, E, 'GaAs')

######################################################################################################################################################

def SolarParticles(T, Tsolmax, rhmean, rhcorr, conflvl, dmgtarget, Eres):
    """
    Solar proton fluence function. Returns the total fluence of solar protons according to the ESP model and the worst-5min solar particle peak fluxes
    according to the CREME96 model.
    Input:
        - T:         mission epochs [yr] (1D array of floats)
        - Tsolmax:   mission time during solar maximum conditions [yr] (float) (None: calculated from solar cycle data)
        - rhmean:    mission-averaged heliocentric distance [AU] (float)
        - rhcorr:    heliocentric distance correction? (boolean)
        - conflvl:   confidence level [%] (float)
        - dmgtarget: target material for radiation damage assessment (Si or GaAs) (str)
        - Eres:      energy resolution (int)
    Output:
        - SP:        solar particles (dictionary of RadPart objects) with:
                     - SPR: solar protons (RadPart object) with ESP model mission omnidirectional integral [cm-2] and differential [cm-2 MeV-1]
                            fluences in function of kinetic energy (1D arrays of floats [E])
    """
    "SOLAR PROTON FLUENCE"
    "ESP data"
    Phiparams = pd.read_csv("../Data/Radiation sources/Solar particle radiation/ESP fluence/ESP_Phiparameters.csv", header=1)
    highEscale = pd.read_csv("../Data/Radiation sources/Solar particle radiation/ESP fluence/ESP_highenergyscale.csv", header=1)
    Elow, Phimean, PhiRV = Phiparams.values[:,0], Phiparams.values[:,1], Phiparams.values[:,2]
    Ehigh, flcoeff = highEscale.values[:,0], highEscale.values[:,1]
    
    "Time during high solar activity"
    if Tsolmax is None:
        DeltaT = solar.solmaxtime(T[0], T[-1])
    else:
        DeltaT = Tsolmax
    
    "Heliocentric distance correction"
    rh = rhmean if rhcorr is True else 1.
    
    "Fluence model"
    Edata = np.concatenate((Elow, Ehigh))
    fluencedata = np.zeros(len(Edata))
    for i in range(len(Edata)):
        if i < len(Elow):
            sigma = math.sqrt(math.log(1 + PhiRV[i]/DeltaT))
            mu = math.log(Phimean[i]*DeltaT) - sigma**2/2
            fluencedata[i] = rh**(-2)*np.exp(mu + sigma*math.sqrt(2)*special.erfinv(2*conflvl/100 - 1))
        else:
            fluencedata[i] = flcoeff[i-len(Elow)]*fluencedata[len(Elow)-1]
    
    "Omnidirectional integral and differential fluences"
    Epr = np.logspace(-1, 3, Eres)
    oifluence = funs.loginterp(Edata, fluencedata, Epr)
    odfluence = -np.gradient(oifluence, Epr)
    
    "Solar proton fluence RadPart"
    SPR = RadPart('Solar protons', 1, 1.008, 938.272, Epr, niel=True, target=dmgtarget, scdegr=True)
    setattr(SPR, 'oifluence', oifluence); setattr(SPR, 'odfluence', odfluence)
    
    "Solar particle dictionary"
    SP = {}
    SP['SPR'] = SPR
    
    return SP

######################################################################################################################################################

def AP8AE8(t, r, Ep, models):
    """
    AP8, AE8 flux models function. Returns the flux of particles trapped in Earth's radiation belts given by the selected NASA's AP8/AE8 model with
    Schmidt's eccentric dipole field model using data generated by the RADBELT code.
    Input:
        - t:         orbital time [s] (1D array of floats)
        - r:         orbital position vector [spherical geographic] [m, rad, rad] in time (2D array of floats: [t, [r, theta, phi]])
        - Ep:        kinetic energies of the trapped particles [MeV] (2D array of floats [models,E])
        - models:    trapped particle models (AP8MAX, AP8MIN, AE8MAX, AE8MIN) (1D array of strs)
    Output:
        - Es:        kinetic energy with non-zero fluxes [MeV] (dictionary of 1D arrays of floats)
        - iodfluxes: AP-8/AE-8 model trapped proton/electron instantaneous omnidirectional differential fluxes [cm-2 s-1 MeV-1]
                     (dictionary of 2D arrays of floats [t,E])
        - oodfluxes: AP-8/AE-8 model trapped proton/electron orbit-averaged omnidirectional differential fluxes [cm-2 s-1 MeV-1]
                     (dictionary of 1D array of floats [E])
    """
    Es, iodfluxes, oodfluxes = {}, {}, {}
    data = {x: np.load("../Data/Radiation sources/Trapped particle radiation/" + x + "data.npy") for x in ['L', 'B_B0', 'Epr', 'Eel']}
    for m in range(len(models)):
        "Raw kinetic energies"
        E0 = Ep[m]
        
        "Geomagnetic field model"
        field = geomag.GSFC12field if models[m] =='AP8MAX' else geomag.JC1960field
        
        "Geomagnetic eccentric dipole"
        _, D_cart, rN, _ = geomag.EccDipole(field)
        
        "AP8/AE8 flux data"
        data[models[m] + 'flux'] = np.load("../Data/Radiation sources/Trapped particle radiation/" + models[m].lower() + "data.npy")
        fluxfun = interpolate.RegularGridInterpolator((data['L'], data['B_B0'], data['Epr' if 'P' in models[m] else 'Eel']),
                                                      data[models[m] + 'flux'], bounds_error=False, fill_value=None)
        
        "Geomagnetic field intensity"
        B = np.linalg.norm(geomag.MagneticField(r, field), axis=1)
        
        "Geomagnetic coordinates"
        rm = geomag.MagneticCoordinates(r, D_cart, rN)
        
        "McIlwain parameter (L) and field at equator (B0)"
        L = rm[:,0]/6371.2e3/np.sin(rm[:,1])**2
        B0 = 0.311653e-4/L**3
        
        "Raw instantaneous omnidirectional integral flux"
        ioiflux0 = fluxfun(np.hstack((np.repeat(L, len(E0))[:,None], np.repeat(B/B0, len(E0))[:,None],
                                      np.tile(E0, len(t))[:,None]))).reshape((len(t),len(E0)))
        
        "Non-zero fluxes"
        nonzero, = np.where(np.all(ioiflux0 == 0., axis=0))
        if len(nonzero) == 0:
            E, ioiflux = E0, ioiflux0
        else:
            i0 = nonzero[0]
            if i0 == 0:
                E, ioiflux = E0, ioiflux0
            else:
                E = np.logspace(math.log10(E0[0]), math.log10(E0[i0-1]), len(E0))
                ioiflux = funs.loginterp(E0[:i0], ioiflux0[:,:i0], E)
        
        "Instantaneous and orbit-averaged omnidirectional differential flux"
        Es[models[m]] = E
        iodflux = -np.gradient(ioiflux, E, axis=1)
        oodflux = np.trapz(iodflux, t, axis=0)/(t[-1] - t[0]) if len(t) > 1 else iodflux[0]
        iodfluxes[models[m]] = iodflux
        oodfluxes[models[m]] = oodflux
        
    return Es, iodfluxes, oodfluxes

######################################################################################################################################################

def TrappedParticles(T, orbit, t, r, dmgtarget, Eres):
    """
    Trapped particles flux function. Returns the flux and fluence of protons and electrons trapped in Earth's radiation belts according to:
        - Protons:   AP8 model
        - Electrons: IGE2006 upper (GEO), MEOv2 upper (GNSS/NAV), AE8 (other) models
    Input:
        - T:         mission epochs [yr] (1D array of floats)
        - orbit:     type of orbit (GEO, GNSS/NAV or other) (str)
        - t:         orbital time [s] (1D array of floats)
        - r:         orbital position vector [spherical geographic] [m, rad, rad] in time (2D array of floats: [t, [r, theta, phi]])
        - dmgtarget: target material for radiation damage results (Si or GaAs) (str)
        - Eres:      energy resolution (number of particle energy points) (int)
    Output:
        - TP:    trapped particles (dictionary of RadPart objects) with:
                 - TPR: trapped protons (RadPart object) with AP8 model instantaneous, orbit-averaged and mission-averaged differential fluxes
                        [cm-2 s-1 MeV-1] and incident differential fluence [cm-2 MeV-1] in function of kinetic energy (3D array of floats [T,t,E]),
                        (2D array of floats [T,E]), (1D arrays of floats [E])
                 - TEL: trapped electrons (RadPart object) with IGE2006 upper, MEOv2 upper or AE8 model (GEO, GNSS/NAV or other) instantaneous,
                        orbit-averaged and mission-averaged differential fluxes [cm-2 s-1 MeV-1] and incident differential fluence
                        [cm-2 MeV-1] in function of kinetic energy (3D array of floats [T,t,E]), (2D array of floats [T,E]), (1D arrays of floats [E])
    """
    "Interplanetary trajectories (no trapped particles)"
    if orbit == 'interplanetary':
        TPR = RadPart('Trapped protons', 1, 1.008, 938.272, np.array([0.1, 400.]))
        setattr(TPR, 'mdflux', np.zeros(2)); setattr(TPR, 'dfluence', np.zeros(2))
        
        TEL = RadPart('Trapped electrons', -1, 0.0005486, 0.511, np.array([9.2e-4, 7.]))
        setattr(TEL, 'mdflux', np.zeros(2)); setattr(TEL, 'dfluence', np.zeros(2))
        
    else:
        "Solar activity"
        DTsolmin, solact = [], []
        for J in range(len(T)):
            _, Tsolmin, Tsolmax = solar.cycle(T[J])
            DeltaTsolmin = int(round(T[J] - Tsolmin))
            DTsolmin.append(DeltaTsolmin - 11 if DeltaTsolmin >= 5 else DeltaTsolmin)
            solact.append('MAX' if (-2.5 <= T[J] - Tsolmax <= 4.0) else 'MIN')
        solmdl = np.unique(solact)
        
        "Models"
        partmdl = ['AP8'] if orbit in ['GEO', 'GNSS', 'NAV'] else ['AP8', 'AE8']
        models = np.char.add(np.repeat(partmdl, len(solmdl)), np.tile(solmdl, len(partmdl)))
        
        "Kinetic energies"
        Epr0 = np.logspace(-1, math.log10(400.0), Eres)
        Eel0 = np.logspace(math.log10(0.04), math.log10(7.0), Eres)
        Ep = np.array([Epr0]*len(solmdl) + ([] if orbit in ['GEO', 'GNSS', 'NAV'] else [Eel0]*len(solmdl)))
        
        "Instantaneous and orbit-averaged omnidirectional differential fluxes"
        E, iodfluxes, oodfluxes = AP8AE8(t, r, Ep, models)
        
        
        "PROTONS"
        "Instantaneous, orbit-averaged, mission-averaged and maximum incident differential fluxes"
        Epr = E[models[0]]
        idfluxpr, odfluxpr = np.zeros((len(T), len(t), len(Epr))), np.zeros((len(T), len(Epr)))
        for J in range(len(T)):
            idfluxpr[J] = iodfluxes['AP8'+solact[J]]
            odfluxpr[J] = oodfluxes['AP8'+solact[J]]
        mdfluxpr = np.trapz(odfluxpr, T, axis=0)/(T[-1] - T[0])
        
        "Incident differential fluence"
        dfluencepr = mdfluxpr*(T[-1] - T[0])*365*24*3600
        
        "RadPart object"
        TPR = RadPart('Trapped protons', 1, 1.008, 938.272, Epr, niel=True, target=dmgtarget, scdegr=True)
        setattr(TPR, 'idflux', idfluxpr); setattr(TPR, 'odflux', odfluxpr); setattr(TPR, 'mdflux', mdfluxpr); setattr(TPR, 'dfluence', dfluencepr)
        
        
        "ELECTRONS"
        if orbit in ['GEO', 'GNSS', 'NAV']:
            "Orbit-averaged directional differential flux data"
            if orbit == 'GEO':
                TELflux = pd.read_csv("../Data/Radiation sources/Trapped particle radiation/IGE2006upper.csv", header=1)
            else:
                TELflux = pd.read_csv("../Data/Radiation sources/Trapped particle radiation/MEOv2upper.csv", header=1)
            Edata = TELflux.values[:,0]
            
            "Instantaneous and orbit-averaged incident differential fluxes"
            Eel = np.logspace(math.log10(min(Edata[0], Eel0[0])), math.log10(max(Edata[-1], Eel0[-1])), Eres)
            idfluxel, odfluxel = np.zeros((len(T), len(t), len(Eel))), np.zeros((len(T), len(Eel)))
            for J in range(len(T)):
                odfluxel[J] = 4*math.pi*funs.loginterp(Edata, TELflux.values[:,DTsolmin[J]+7], Eel)
                idfluxel[J] = odfluxel[J]*np.ones((len(t), 1))
        else:
            "Instantaneous and orbit-averaged incident differential fluxes"
            Eel = E[models[-1]]
            idfluxel, odfluxel = np.zeros((len(T), len(t), len(Eel))), np.zeros((len(T), len(Eel)))
            for J in range(len(T)):
                idfluxel[J] = iodfluxes['AE8'+solact[J]]
                odfluxel[J] = oodfluxes['AE8'+solact[J]]
        
        "Mission-averaged, maximum incident differential fluxes and incident differential fluence"
        mdfluxel = np.trapz(odfluxel, T, axis=0)/(T[-1] - T[0])
        dfluenceel = mdfluxel*(T[-1] - T[0])*24*365*3600
        
        "RadPart object"
        TEL = RadPart('Trapped electrons', -1, 0.0005486, 0.511, Eel, niel=True, target=dmgtarget, scdegr=True)
        setattr(TEL, 'idflux', idfluxel); setattr(TEL, 'odflux', odfluxel); setattr(TEL, 'mdflux', mdfluxel); setattr(TEL, 'dfluence', dfluenceel)
    
    
    "RadPart dictionary"
    TP = {'TPR': TPR, 'TEL': TEL}
    
    return TP

######################################################################################################################################################

def GCRParticles(T, nstdev, dmgtarget, Eres):
    """
    Galactic cosmic rays flux function. Returns the fluxes of galactic cosmic particles Z = 1 - 92 (H through U) according to the ISO15390 model.
    Epochs in 2020 and beyond are translated back 22*n years (to solar cycles nº 23 and nº 24) for solar activity assessment [SPENVIS].
    Input:
        - T:         mission epochs [yr] (1D array of floats)
        - nstdev:    number of flux standard deviations (int)
        - dmgtarget: target material for radiation damage results (Si or GaAs) (str)
        - Eres:      energy resolution (number of particle energy points) (int)
    Output:
        - GCR:       galactic cosmic rays (dictionary of RadPart objects) with:
                     - <ion> gcr: galactic cosmic ions (RadPart objects) with ISO15390 model epoch-instantaneous omnidirectional differential fluxes
                                  [cm-2 s-1 (MeV/nucl)-1] in function of kinetic energy (2D arrays of floats [T,E])
    """
    "GCR data"
    ISO15390 = pd.read_csv("../Data/Radiation sources/Galactic cosmic rays/ISO15390parameters.csv", header=1)
    Z, elems, A, C, sigmaC, gamma, sigmagamma, alpha, sigmaalpha = (ISO15390.values[:,0].astype(int), ISO15390.values[:,1].astype(str),
                                                                       ISO15390.values[:,2].astype(float), ISO15390.values[:,3].astype(float),
                                                                       ISO15390.values[:,4].astype(float), ISO15390.values[:,5].astype(float),
                                                                       ISO15390.values[:,6].astype(float), ISO15390.values[:,7].astype(float),
                                                                       ISO15390.values[:,8].astype(float))
    
    "Kinetic energy and specific mass"
    E = np.logspace(-3, 2, Eres) # [GeV/nucl]
    m = 0.939*np.ones(len(Z))
    m[0] = 0.938272
    
    "Sun polar magnetic field parameters"
    Tsr = [1958.21, 1968.87, 1979.96, 1989.46, 2000.71, 2011.3] # Sun's polar magnetic field sign-reversal epoch (n = 19 - 24)
    Tplus = 15.0 # [months]
    
    "Flux model"
    eodfluxes = {k: np.zeros((len(T), len(E))) for k in range(len(Z))}
    for J in range(len(T)):
        "Solar cycle"
        T0 = T[J] - 22.*(1 + math.floor((T[J] - 2020.)/22)) if T[J] >= 2020. else T[J]
        N0, _, _ = solar.cycle(T0)
        
        "Solar activity"
        Wmin0, Wmax, W = solar.activity(T0)
        _, T1, _ = solar.cycle(None, N=N0+1)
        Wmin1, _, _ = solar.activity(T1 - 22.*(1 + math.floor((T1 - 2020.)/22)) if T1 >= 2020. else T1)
        Wmin = min([Wmin0, Wmin1])
        _, _, WdeltawT = solar.activity(T0 - 16/12) # (deltawT = 16 months)
        S = 1 if T0 >= Tsr[N0-19] else -1
        M = (-1)**(N0-1)*S*(1 - ((W - Wmin)/(Wmax - Wmin))**2.7)
        tau = (-1)**N0*((WdeltawT - Wmin)/Wmax)**0.2
        
        for k in range(len(Z)):
            "Particle rigidity and relative velocity ratio"
            R = A[k]/Z[k]*np.sqrt(E*(E + 2*m[k]))
            beta = np.sqrt(E*(E + 2*m[k]))/(E + m[k])
            
            "Flux lag"
            Tminus = 7.5*R**-0.45 # [months]
            DeltaT = 0.5*(Tplus + Tminus) + 0.5*(Tplus - Tminus)*tau # [months]
            
            "Effective heliosphere modulation potential"
            WDeltaT = np.zeros(len(E))
            for i in range(len(E)):
                _, _, WDeltaT[i] = solar.activity(T0 - DeltaT[i]/12)
            R0 = 0.37 + 3e-4*WDeltaT**1.45
            
            "Particle rigidity spectra"
            Delta = 5.5 + 1.13*M*beta*R/R0*np.exp(-beta*R/R0)
            phi = C[k]*beta**alpha[k]/R**gamma[k]*(R/(R + R0))**Delta # [m-2 sr-1 s-1 GeV-1]
            
            "Instantaneous omnidirectional differential flux at mission epochs"
            F = phi*A[k]/Z[k]*1e-3/beta/1e4 # [cm-2 sr-1 s-1 (MeV/nucl)-1]
            sigmaF_F = 0 if C[k] == 0 else np.sqrt((sigmaC[k]/C[k])**2 + (np.log(beta)*sigmaalpha[k])**2 + (np.log(R)*sigmagamma[k])**2)
            eodfluxes[k][J] = F*(1 + nstdev*sigmaF_F)*4*math.pi # [cm-2 s-1 (MeV/nucl)-1]
    
    "GCR RadPart dictionary"
    GCR = {}
    for k in range(len(Z)):
        "GCR particle RadPart"
        part = RadPart('Galactic cosmic ' + elems[k], Z[k], A[k], m[k]*A[k]*1e3, E*1e3, target=dmgtarget)
        
        "Epoch-instantaneous omnidirectional differential flux"
        eodflux = eodfluxes[k]
        setattr(part, 'eodflux', eodflux)
        
        "GCR particle dictionary"
        GCR[elems[k] + ' gcr'] = part
    
    return GCR

######################################################################################################################################################