"""
Auxiliary functions module. It includes auxiliary functions for interpolation, integration and plotting.
"""

import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt

######################################################################################################################################################

def loginterp(xdata, ydata, x=None, fun=False, kind='loglog', fill='extrapolate'):
    """
    Logarithmic interpolation function.
    Input:
        - xdata: x data (1D array of floats)
        - ydata: y data (1D array of floats)
        - x:     x values to which interpolate (1D array of floats) (None if fun is True)
        - fun:   return interpolation function? (boolean) (defaults to False: no, return interpolated data instead)
        - kind:  kind of interpolation (loglog, semilogx, semilogy) (str) (defaults to log-log)
        - fill:  fill value for interpolation (defaults to extrapolation)
    Output:
        - yfun:  log-interpolated y function (function) (if fun=True)
        - y:     log-interpolated y values (1D array of floats [x]) (otherwise)
    """
    if kind == 'loglog':
        if isinstance(fill, float):
            fill = np.nan if fill == 0. else np.log10(fill)
        interpfun = interpolate.interp1d(np.log10(xdata), np.log10(ydata), fill_value=fill, bounds_error=False)
        yfun = lambda x: 10**interpfun(np.log10(x))
        
    elif kind == 'semilogy':
        if isinstance(fill, float):
            fill = np.nan if fill == 0. else np.log10(fill)
        interpfun = interpolate.interp1d(xdata, np.log10(ydata), fill_value=fill, bounds_error=False)
        yfun = lambda x: 10**interpfun(x)
        
    elif kind == 'semilogx':
        interpfun = interpolate.interp1d(np.log10(xdata), ydata, fill_value=fill, bounds_error=False)
        yfun = lambda x: interpfun(np.log10(x))
    
    if fun is True:
        return yfun
    else:
        return np.nan_to_num(yfun(x))

######################################################################################################################################################

def integralfrom(y, x):
    """
    "From value" integral function. Returns the numerical integral from each value to infinity (PDF-like) of the given input values with
    logarithmic interpolation.
    Input:
        - y: y values (1D array of floats)
        - x: x values (1D array of floats)
    Output:
        - Y: integral of y(x) from x to inf: Y(>x) (1D array of floats [x])
    """
    x0, y0 = x[y > 0.], y[y > 0.]
    yfun = loginterp(x0, y0, fun=True)
    Y0 = np.zeros(len(x0))
    for i in range(len(x0) - 1):
        x1 = np.logspace(np.log10(x0[i]), np.log10(x0[-1]), max(len(x0), 50))
        Y0[i] = np.trapz(yfun(x1), x1)
    Y = np.concatenate((Y0[0]*np.ones(x[y <= 0.].shape), Y0))
    
    return Y

######################################################################################################################################################

def plot(pltfun, X, Y, C, LS, leglab, axlab, title=None, legloc='best', grid=True):
    """
    Plot function.
    Input:
        - X:      x data (list of arrays/lists)
        - Y:      y data (list of arrays/lists)
        - C:      line colours (list of strings/tuples of floats)
        - LS:     linestyles (list of strings)
        - leglab: legend labels (list of strings)
        - legloc: legend location (string)
        - axlab:  axes labels (list of strings)
        - title:   title (string)
        - pltfun: plot function (function)
    """
    plt.figure()
    [pltfun(X[i], Y[i], color=C[i], linestyle=LS[i], label=leglab[i], linewidth=2.) for i in range(len(X))]
    if any(leglab):
        plt.legend(loc=legloc)
    plt.xlabel(axlab[0])
    plt.ylabel(axlab[1])
    plt.title(title)
    plt.grid(grid, which='both', alpha=0.5)

######################################################################################################################################################