B
    ���^$  �               @   sL   d Z ddlZddlZddlmZ ddlZddlZdd� Z	dd� Z
dd	� ZdS )
z�
Shielding module. It includes the necessary functions to apply the geomagnetic and material shielding to all radiation particles of the mission.
�    N)�	integratec                s  |dkrt �t���n8dddt �|dd�df d d �|dd�df     }�fdd	�|�� D �}|d
k�rptjtjt �| �d�\}	}
}}t�	||
|��
� \}}}tjd | }|d t �|�d  }x�tt���D ]�}|| dk r�t�|	|| || |�\�� � �� � ��  ��� ���fdd��xB|�� D ]6}|jdk�r2t ��fdd�|jD ��||j |< �q2W q�W �x�|�� D �]�}||j |dd�df  }t��dk�r�t j|�dd��d �d   n|d }t|d|� t|d|� |jdk�r&|j| }t�||j�}t|d|� t|d|� d|jk�r||jdd�ddd�f |ddd�dd�f  }t jt��dk�r�t j|�dd��d �d   n|dd�ddd�f | dd�| d | d   }t|d|� t|d|� || d | d   d d d }t|d|� �q|W dS ) a]  
    Earth shadow and magnetic shielding function. Computes the exposure factor (fraction of solid angle from which particles can arrive) and incident
    (geo-shielded) fluxes and fluences of the given radiation particles, using:
        - Schmidt's eccentric dipole model with IGRF13 field
        - Stormer's geomagnetic shielding theory with SPENVIS' stormy magnetosphere correction
        - SPENVIS' Earth shadow attenuation
    Input:
        - T:        mission epochs [yr] (1D array of floats)
        - t:        orbital time [s] (1D array of floats)
        - r:        orbital position vector [spherical geographic] [m, rad, rad] in time (2D array of floats: [t, [r, theta, phi]])
        - trjtype:  type of trayectory (general, GNSS/NAV, GEO, interplanetary, custom) (str)
        - magsh:    geomagnetic shielding? (boolean)
        - stormy:   stormy geomagnetic conditions? (boolean)
        - radparts: radiation particles (dictionary of RadPart objects)
    Output:
        - Exposure factors and incident (geo-shielded) fluxes and fluences (as attributes of the corresponding RadPart objects)
    �interplanetary�   g      �?Nr   �   g   ��u�Bc                s(   i | ] }t �t� �t|j�f�|j�qS � )�np�ones�len�P�name)�.0�part)�tr   ��/home/tgateau/git/nanostar_dev/doc/specifications/Specification_NSS_Core/contributions/radiation/wetransfer-0a1391/Codes/shielding.py�
<dictcomp>#   s    z GeoShielding.<locals>.<dictcomp>T)�fieldargg    �MXAg      @c          	      sH   t �| | �k | �k�|   ko$�kn  gdd� dd� � �fdd�g�S )Nc             S   s   dS )Ng        r   )�pr   r   r   �<lambda>4   �    z0GeoShielding.<locals>.<lambda>.<locals>.<lambda>c             S   s   dS )Ng      �?r   )r   r   r   r   r   4   r   c                s,   dt �tjd �| � k d  d � d S )Nr   r   �����)r   �cos�math�pi)r   )�Pc�thetar   r   r   5   r   )r   �	piecewise)r   )r   �Pcmax�Pcminr   r   r   r   4   s   6zGeoShielding.<locals>.<lambda>)�TPR�TELc                s   g | ]}� |��qS r   r   )r   r   )�fgmfunr   r   �
<listcomp>8   s    z GeoShielding.<locals>.<listcomp>)�axisr   �ifexp�mfexpzSolar protons�dfluence�ifluenceZcosmic�idflux�mdflux�   im  i  )r   r   r	   �sqrt�values�geomag�	EccDipole�IGRF13field�mean�MagneticCoordinates�	transposer   r   r   �range�MagneticCutoff�min�maxr   �arrayr
   �trapz�setattr�	odfluence�funs�integralfrom�E�eodflux)�Tr   �r�trjtype�magsh�stormy�radpartsZfshZfgm�M0�D_cart�rN�_�rm�thetam�lambdam�L�jr   r#   r$   r%   r&   r'   r(   r   )r   r   r   r    r   r   r   �GeoShielding   s@    P
.6 
 .`  rM   c                s�  |dkrdnd�t �dtjd d�}�fdd��| d	k�r&�d
 }	t �ddddgddddgddddgg���fdd�}
d|
d� |
d�d|
d�|	 |
d�    �dd�  d�  �����fdd�}|dkr�tj||�t �|�ddd�df  |dd�}n|d�dd�ddd�f }��
 | }n����fdd�� d}d | |d! d" d# d  d$ �� ��fd%d��� ��
fd&d��	����	��fd'd�}|dk�r�tj||�t �|�ddd�df  |dd�}n|d�dd�ddd�f }|S )(a  
    Aluminium-equivalent shielding function. Returns the transmitted differential flux or fluence of the given radiation particle, i.e. after passing
    through the given Al-equivalent shielding, according to:
        - Electrons:        method of Tabata et al. 1993 [OMERE (similar)]: empirical Ebert-type transmission formula
        - Protons and ions: method of Adams 1983 [SPENVIS, CREME, OMERE] (no fragmentation)
    Input:
        - Zi:     atomic number of the incident particle (float)
        - Ai:     mass number of the incident particle [u] (float)
        - Ei:     kinetic energy of the incident particle [MeV/nucl] (1D array of floats)
        - dfli:   incident differential flux or fluence of the incident particle [cm-2 s-1 (MeV/u)-1] (1D array of floats [E])
        - SAl:    total stopping power of the incident particle in aluminium [MeV cm2 g(Al)-1] (1D array of floats [E])
        - RpAl:   projected range of the incident particle in aluminium [g(Al) cm-2] (1D array of floats [E])
        - tAl:    Al-equivalent shielding thickness [g(Al) cm-2] (1D array of floats)
        - shconf: shielding configuration (planar or spherical) (str)
    Output:
        - dflt:   transmitted (shielded) differential flux or fluence of the incident particle (2D array of floats [tAl,E])
    Zplanarg      �?g      �?g        r   �2   c                s   � d d �d f t �| � S )N)r   r   )r   )�tAlr   r   r   o   r   zAlEqShielding.<locals>.<lambda>r   g'1�Z�?g���(\�@g�Q���@g������6@gffffff�?go��ʡտg=
ףp=ڿgq=
ףp��g�p=
ף��g333333��g��(\���?gw��/��?g333333�?c                s2   � d| f d� d| f � d| f t �d�    S )Nr   �   r   r   )r   �log)�i)�b0r   r   r   v   r   r   r   �   c                s.   t �� �| �d d �d d �d f �  �  �S )N)r   �exp)r   )�RpAl�alpha�beta�teffr   r   r   {   r   N)r"   c          	      s(   t ��� ��| �d d �d d �d f  �S )N)r:   �	loginterp)r   )�EirV   rY   r   r   r   �   r   g�Wʅ��Dg�}����:gUUUUUU�?g      @g�������?�   c                s   t ���� | ��S )N)r:   rZ   )r   )�E0r[   �SAlr   r   r   �   r   c                s   t ���� | ��S )N)r:   rZ   )r   )r]   r[   �dflir   r   r   �   r   c                s>   ��| � �| � �  t �� �| � �d d �d d �d f  S )N)r   rU   )r   )r^   �SAl0�cAl�dfl0�firY   r   r   r   �   r   )r   �linspacer   r   r6   r   Zsimps�sin)�Zi�Air[   r_   r^   rV   rO   �shconfZthetai�tau�bZetatfunZetatZdflt�NAZdfltfunr   )r]   r[   rV   r^   r`   rW   rS   rX   ra   rb   r_   rc   rO   rY   r   �AlEqShieldingV   s0    
(,. 
.rl   c          
   C   sv   xp|� � D ]d}t|d�r
t�|jdk�r@t�t| �t|j�f�n t|j	|j
|j|j|j|j| |�}t|d|� q
W dS )ar  
    Material shielding function. Computes the transmitted (behind shielding) differential flux or fluence of the given radiation particles for the
    given Al-equivalent shielding.
    Input:
        - tAl:      Al-equivalent shielding thickness [g(Al) cm-2] (1D array of floats)
        - shconf:   shielding configuration (planar or spherical) (str)
        - radparts: radiation particles (dictionary of RadPart objects)
    Output:
        - Transmitted (shielded) differential fluence of the incident particles (2D arrays of floats [tAl,E]) (as attribute 'dfluencet' of the
          corresponding RadPart objects)
    r%   g        �	dfluencetN)r+   �hasattrr   �allr%   �zerosr	   r<   rl   �Z�Ar^   rV   r8   )rO   rh   rC   r   rm   r   r   r   �MaterialShielding�   s
    
("rs   )�__doc__r   �numpyr   �scipyr   r,   �	otherfunsr:   rM   rl   rs   r   r   r   r   �<module>   s   ID