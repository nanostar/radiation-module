"""
Shielding module. It includes the necessary functions to apply the geomagnetic and material shielding to all radiation particles of the mission.
"""

import math
import numpy as np
from scipy import integrate
import geomag
import otherfuns as funs

######################################################################################################################################################

def GeoShielding(T, t, r, trjtype, magsh, stormy, radparts):
    """
    Earth shadow and magnetic shielding function. Computes the exposure factor (fraction of solid angle from which particles can arrive) and incident
    (geo-shielded) fluxes and fluences of the given radiation particles, using:
        - Schmidt's eccentric dipole model with IGRF13 field
        - Stormer's geomagnetic shielding theory with SPENVIS' stormy magnetosphere correction
        - SPENVIS' Earth shadow attenuation
    Input:
        - T:        mission epochs [yr] (1D array of floats)
        - t:        orbital time [s] (1D array of floats)
        - r:        orbital position vector [spherical geographic] [m, rad, rad] in time (2D array of floats: [t, [r, theta, phi]])
        - trjtype:  type of trayectory (general, GNSS/NAV, GEO, interplanetary, custom) (str)
        - magsh:    geomagnetic shielding? (boolean)
        - stormy:   stormy geomagnetic conditions? (boolean)
        - radparts: radiation particles (dictionary of RadPart objects)
    Output:
        - Exposure factors and incident (geo-shielded) fluxes and fluences (as attributes of the corresponding RadPart objects)
    """
    "Earth shadow attenuation factor"
    fsh = np.ones(len(t)) if trjtype == 'interplanetary' else 1 - 1/2*(1 - np.sqrt(r[:,0]**2 - (6371.2e3)**2)/r[:,0])
    
    "Geomagnetic attenuation"
    fgm = {part.name: np.ones((len(t), len(part.P))) for part in radparts.values()}
    if magsh is True:
        "Eccentric dipole"
        M0, D_cart, rN, _ = geomag.EccDipole(geomag.IGRF13field, fieldarg=np.mean(T))
        
        "Geomagnetic coordinates"
        rm, thetam, _ = geomag.MagneticCoordinates(r, D_cart, rN).transpose()
        lambdam = math.pi/2 - thetam
        L = rm/6371.2e3/np.cos(lambdam)**2
        
        for j in range(len(t)):
            if L[j] < 5.:
                "Geomagnetic cut-off"
                theta, Pc = geomag.MagneticCutoff(M0, rm[j], lambdam[j], stormy)
                Pcmin, Pcmax = Pc.min(), Pc.max()
                
                "Geomagnetic attenuation factor"
                fgmfun = lambda p: np.piecewise(p, [p < Pcmin, p > Pcmax, Pcmin <= p <= Pcmax], [lambda p: 0., lambda p: 1.,
                                                lambda p: (1 - np.cos((math.pi/2 + theta[p >= Pc][-1])/2))/2])
                for part in radparts.values():
                    if part.name not in ['TPR', 'TEL']:
                        fgm[part.name][j] = np.array([fgmfun(p) for p in part.P])
    
    
    "Exposure factors and incident fluxes/fluences"
    for part in radparts.values():
        "Instantaneous and mission-averaged exposure factors"
        ifexp = fgm[part.name]*fsh[:,None]
        mfexp = np.trapz(ifexp, t, axis=0)/(t[-1] - t[0]) if len(t) > 1 else ifexp[0]
        setattr(part, 'ifexp', ifexp); setattr(part, 'mfexp', mfexp)
        
        "SOLAR PROTONS"
        if part.name == 'Solar protons':
            "Incident differential and integral fluences"
            dfluence = part.odfluence*mfexp
            ifluence = funs.integralfrom(dfluence, part.E)
            setattr(part, 'dfluence', dfluence); setattr(part, 'ifluence', ifluence)
        
        "GALACTIC COSMIC RAYS"
        if 'cosmic' in part.name:
            "Instantaneous and mission-averaged incident differential fluxes"
            idflux = part.eodflux[:,None,:]*ifexp[None,:,:]
            mdflux = np.trapz(np.trapz(idflux, t, axis=1)/(t[-1] - t[0]) if len(t) > 1 else idflux[:,0,:], T, axis=0)/(T[-1] - T[0])
            setattr(part, 'idflux', idflux); setattr(part, 'mdflux', mdflux)
            
            "Incident differential fluence"
            dfluence = mdflux*(T[-1] - T[0])*24*365*3600
            setattr(part, 'dfluence', dfluence)

######################################################################################################################################################

def AlEqShielding(Zi, Ai, Ei, dfli, SAl, RpAl, tAl, shconf):
    """
    Aluminium-equivalent shielding function. Returns the transmitted differential flux or fluence of the given radiation particle, i.e. after passing
    through the given Al-equivalent shielding, according to:
        - Electrons:        method of Tabata et al. 1993 [OMERE (similar)]: empirical Ebert-type transmission formula
        - Protons and ions: method of Adams 1983 [SPENVIS, CREME, OMERE] (no fragmentation)
    Input:
        - Zi:     atomic number of the incident particle (float)
        - Ai:     mass number of the incident particle [u] (float)
        - Ei:     kinetic energy of the incident particle [MeV/nucl] (1D array of floats)
        - dfli:   incident differential flux or fluence of the incident particle [cm-2 s-1 (MeV/u)-1] (1D array of floats [E])
        - SAl:    total stopping power of the incident particle in aluminium [MeV cm2 g(Al)-1] (1D array of floats [E])
        - RpAl:   projected range of the incident particle in aluminium [g(Al) cm-2] (1D array of floats [E])
        - tAl:    Al-equivalent shielding thickness [g(Al) cm-2] (1D array of floats)
        - shconf: shielding configuration (planar or spherical) (str)
    Output:
        - dflt:   transmitted (shielded) differential flux or fluence of the incident particle (2D array of floats [tAl,E])
    """
    "Incidence factor"
    fi = 0.5 if shconf == 'planar' else 1.
    
    "Incidence angle"
    thetai = np.linspace(0., math.pi/2, 50) # [theta]
    
    "Effective shielding thickness"
    teff = lambda theta: tAl[:,None]/np.cos(theta) # [tAl,theta]
    
    # Electrons
    if Zi == -1:
        "Transmission parameters"
        tau = Ei/0.511 # [E]
        b0 = np.array([[4.89, 5.73, 22.6, 1.65],[-0.338, -0.41, -0.92, -0.13],[-0.075, 0.17, 0.163, 0.15]])
        b = lambda i: b0[0,i]*13**(b0[1,i] + b0[2,i]*math.log(13))
        beta = 1 + b(0) + b(1)/(1 + (b(2)/tau)**b(3)) # [E]
        alpha = (1 - 1/beta)**(1 - beta) # [E]
        
        "Transmission function"
        etatfun = lambda theta: np.exp(-alpha*(teff(theta)[:,:,None]/RpAl)**beta) # [tAl,theta,E]
        if shconf == 'planar':
            etat = integrate.simps(etatfun(thetai)*np.sin(thetai)[None,:,None], thetai, axis=1) # [tAl,E]
        else:
            etat = etatfun(0.)[:,0,:] # [tAl,E]
        
        "Transmitted (shielded) differential flux or fluence"
        dflt = fi*dfli*etat # [tAl,E]
    
    # Protons and ions
    else:
        "Transmitted (shielded) kinetic energy"
        E0 = lambda theta: funs.loginterp(RpAl, Ei, RpAl + teff(theta)[:,:,None]) # [tAl,theta,E]
        
        "Transmission parameters"
        NA = 6.02214076e23
        cAl = 5e-26*NA*(Ai**(1/3) + 27**(1/3) - 0.4)**2/27
        SAl0 = lambda theta: funs.loginterp(Ei, SAl, E0(theta)) # [tAl,theta,E]
        dfl0 = lambda theta: funs.loginterp(Ei, dfli, E0(theta)) # [tAl,theta,E]
        
        "Transmitted (shielded) differential flux or fluence function"
        dfltfun = lambda theta: fi*dfl0(theta)*SAl0(theta)/SAl*np.exp(-cAl*teff(theta))[:,:,None] # [tAl,theta,E]
        if shconf == 'planar':
            dflt = integrate.simps(dfltfun(thetai)*np.sin(thetai)[None,:,None], thetai, axis=1) # [tAl,E]
        else:
            dflt = dfltfun(0.)[:,0,:] # [tAl,E]
    
    return dflt

######################################################################################################################################################

def MaterialShielding(tAl, shconf, radparts):
    """
    Material shielding function. Computes the transmitted (behind shielding) differential flux or fluence of the given radiation particles for the
    given Al-equivalent shielding.
    Input:
        - tAl:      Al-equivalent shielding thickness [g(Al) cm-2] (1D array of floats)
        - shconf:   shielding configuration (planar or spherical) (str)
        - radparts: radiation particles (dictionary of RadPart objects)
    Output:
        - Transmitted (shielded) differential fluence of the incident particles (2D arrays of floats [tAl,E]) (as attribute 'dfluencet' of the
          corresponding RadPart objects)
    """
    for part in radparts.values():
        "Transmitted (shielded) differential fluence"
        if hasattr(part, 'dfluence'):
            dfluencet = (np.zeros((len(tAl), len(part.E))) if np.all(part.dfluence == 0.) else
                         AlEqShielding(part.Z, part.A, part.E, part.dfluence, part.SAl, part.RpAl, tAl, shconf))
            setattr(part, 'dfluencet', dfluencet)
    
######################################################################################################################################################