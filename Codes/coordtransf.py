"""
Coordinate transformations module. It includes the necessary functions to perform coordinate transformations.
"""

import numpy as np

######################################################################################################################################################

def cart2spher(a):
    """
    Cartesian-spherical coordinate transformation function. Returns the given position vector (in Cartesian coordinates) in spherical coordinates.
    Input:
        - a: position vector to be transformed [Cartesian] [arbitrary length units] (array of floats [x, y, z])
    Output:
        - Transformed vector [spherical] [arbitrary length unit, rad, rad] (array of floats [r, theta, phi])
    """
    x, y, z = a
    r = np.sqrt(x**2 + y**2 + z**2)
    theta = np.arctan2(np.sqrt(x**2 + y**2), z)
    phi = np.arctan2(y, x)
    
    return np.array([r, theta, phi])

######################################################################################################################################################

def spher2cart(a):
    """
    Spherical-cartesian coordinate transformation function. Returns the given position vector (in spherical coordinates) in Cartesian coordinates.
    Input:
        - a: position vector to be transformed [spherical] [arbitrary length unit, rad, rad] (array of floats [r, theta, phi])
    Output:
        - Transformed vector [Cartesian] [arbitrary length units] (array of floats [x, y, z])
    """
    r, theta, phi = a
    x = r*np.sin(theta)*np.cos(phi)
    y = r*np.sin(theta)*np.sin(phi)
    z = r*np.cos(theta)
    
    return np.array([x, y, z])

######################################################################################################################################################

def GEOtoMAG(a, thetaN, phiN):
    """
    GEO-MAG coordinate transformation function. Returns the given vector (in Cartesian geographic coordinates) in Cartesian magnetic coordinates for
    the given dipole axis.
    Input:
        - a:      vector to be transformed [Cartesian GEO] [arbitrary length unit, rad, rad] (array of floats [x, y, z])
        - thetaN: dipole North pole colatitude [rad] (float)
        - phiN:   dipole North pole longitude [rad] (float)
    Output:
        - Transformed vector [Cartesian MAG] [arbitrary length units] (array of floats [x1, y1, z1])
    """
    sin, cos = np.sin, np.cos
    T = np.array([[cos(thetaN)*cos(phiN), cos(thetaN)*sin(phiN), -sin(thetaN)], [-sin(phiN), cos(phiN), 0],
                   [sin(thetaN)*cos(phiN), sin(thetaN)*sin(phiN), cos(thetaN)]])
    
    return np.matmul(T, a)

######################################################################################################################################################

def GEItoGEO(a, phiG):
    """
    GEI-GEO coordinate transformation function. Returns the given vector (in Cartesian geo-inertial coordinates) in Cartesian geographic coordinates
    for the given Greenwich position.
    Input:
        - a:    vector to be transformed [Cartesian GEI] [arbitrary length units] (array of floats [x, y, z])
        - phiG: Greenwich mean sidereal time [rad] (float)
    Output:
        - Transformed vector [Cartesian GEO] [arbitrary length units] (array of floats [x1, y1, z1])
    """
    sin, cos = np.sin, np.cos
    T = np.array([[cos(phiG), -sin(phiG), 0.], [sin(phiG), cos(phiG), 0.], [0., 0., 1.]])
    
    return np.matmul(T, a)

######################################################################################################################################################

def ORBtoGEI(a, i, O, o):
    """
    ORB-GEI coordinate transformation function. Returns the given vector (in Cartesian orbital coordinates) in Cartesian geo-inertial coordinates for
    the given orbital elements.
    Input:
        - a: vector to be transformed [Cartesian GEO] [arbitrary length units] (array of floats [x, y, z])
        - i: inclination [rad] (float)
        - O: longitude of ascending node [rad] (float)
        - o: argument of periapsis [rad] (float)
    Output:
        - Transformed vector [Cartesian GEI] [arbitrary length units] (array of floats [x1, y1, z1])
    """
    s, c = np.sin, np.cos
    T = np.array([[c(O)*c(o) - s(O)*s(o)*c(i), -c(O)*s(o) - s(O)*c(o)*c(i), s(O)*s(i)],
                   [s(O)*c(o) + c(O)*s(o)*c(i), -s(O)*s(o) + c(O)*c(o)*c(i), -c(O)*s(i)], [s(o)*s(i), c(o)*s(i), c(i)]])
    
    return np.matmul(T, a)

######################################################################################################################################################