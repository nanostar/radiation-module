"""
Geomagnetics module. It includes all geomagnetic-related functions: geomagnetic field models, magnetic coordinate transformations, geomagnetic cut-off
of radiation particles, etc.
"""

import math
import numpy as np
import pandas as pd
from scipy import special, interpolate
import coordtransf

######################################################################################################################################################

def JC1960field():
    """
    Jensen & Cain (1960) geomagnetic field model function. Returns the geomagnetic potential spherical coefficients for epoch 1960 according to the
    model of Jensen and Cain.
    Output:
        - n, m, gnm, hnm: orders, degrees and geomagnetic potential spherical coefficients [T] (1D arrays of floats)
    """
    JensenCain = pd.read_csv("../Data/Geomagnetic fields/JensenCain.csv", header=1)
    n, m, gnm, hnm = (JensenCain.values[:,0].astype(int), JensenCain.values[:,1].astype(int),
                      JensenCain.values[:,2]/1e9, JensenCain.values[:,3]/1e9)
    
    return n, m, gnm, hnm

######################################################################################################################################################

def GSFC12field(T=1970.0):
    """
    GSFC12 geomagnetic field model function. Returns the geomagnetic potential spherical coefficients for the given epoch interpolated or extrapolated
    in time from the GSFC12-1966 model.
    Input:
        - T:              epoch [yr] (float) (defaults to 1970.0, standard for trapped proton model AP8-MAX)
    Output:
        - n, m, gnm, hnm: orders, degrees and geomagnetic potential spherical coefficients [T] (1D arrays of floats)
    """
    GSFC12_66 = pd.read_csv("../Data/Geomagnetic fields/GSFC12_66.csv", header=1)
    n, m, g0, h0, g1, h1, g2, h2 = (GSFC12_66.values[:,0].astype(int), GSFC12_66.values[:,1].astype(int), GSFC12_66.values[:,2]/1e9,
                                    GSFC12_66.values[:,3]/1e9, GSFC12_66.values[:,4]/1e9, GSFC12_66.values[:,5]/1e9, GSFC12_66.values[:,6]/1e9,
                                    GSFC12_66.values[:,7]/1e9)
    DeltaT = T - 1966.0
    gnm, hnm = g0 + g1*DeltaT + g2*DeltaT**2, h0 + h1*DeltaT + h2*DeltaT**2
    
    return n, m, gnm, hnm

######################################################################################################################################################

def IGRF13field(T):
    """
    IGRF13 geomagnetic field model function. Returns the geomagnetic potential spherical coefficients for the given epoch interpolated or extrapolated
    in time from the IGRF13 model.
    Input:
        - T:              epoch [yr] (int)
    Output:
        - n, m, gnm, hnm: orders, degrees and geomagnetic potential spherical coefficients [T] (floats)
    """
    IGRF13_g = pd.read_csv("../Data/Geomagnetic fields/IGRF13_gcoeffs.csv", header=1)
    IGRF13_h = pd.read_csv("../Data/Geomagnetic fields/IGRF13_hcoeffs.csv", header=1)
    Tdata = np.linspace(1900.0, 2020.0, 25)
    n, m, gdata, hdata = (IGRF13_g.values[:,1].astype(int), IGRF13_g.values[:,2].astype(int), IGRF13_g.values[:,3:].astype(float)/1e9,
                      IGRF13_h.values[:,3:].astype(float)/1e9)
    gnm, hnm = np.zeros(len(n)), np.zeros(len(n))
    for i in range(len(n)):
        gnm[i] = interpolate.interp1d(Tdata, gdata[i,:], fill_value='extrapolate')(T)
        hnm[i] = interpolate.interp1d(Tdata, hdata[i,:], fill_value='extrapolate')(T)
    
    return n, m, gnm, hnm

######################################################################################################################################################

def EccDipole(field, fieldarg=None):
    """
    Eccentric dipole function. Returns the dipole moment and the positions of the geomagnetic dipole centre and poles in spherical geographic
    coordinates according to the given field model using Schmidt's eccentric dipole model.
    Input:
        - field:    magnetic field model (function)
        - fieldarg: input of magnetic field model (function argument)
    Output:
        - M0:       geomagnetic dipole moment [T m3] (float)
        - D_cart:   eccentric dipole centre position vector [Cartesian geographic] [m] (array of floats: [xD, yD, zD])
        - rN:       North pole position vector [spherical geographic] [m, rad, rad] (array of floats: [rN, thetaN, phiN])
        - rS:       South pole position vector [spherical geographic] [m, rad, rad] (array of floats: [rS, thetaS, phiS])
    """
    RE = 6371.2e3
    n, m, gnm, hnm = field() if fieldarg == None else field(fieldarg)
    
    g10, g11, g20, g21, g22 = gnm[:5]
    h10, h11, h20, h21, h22 = hnm[:5]
    L0 = 2*g10*g20 + math.sqrt(3)*(g11*g21 + h11*h21)
    L1 = -g11*g20 + math.sqrt(3)*(g10*g21 + g11*g22 + h11*h22)
    L2 = -h11*g20 + math.sqrt(3)*(g10*h21 - h11*g22 + g11*h22)
    B0 = math.sqrt(g10**2 + g11**2 + h11**2)
    E = (L0*g10 + L1*g11 + L2*h11)/(4*B0**2)
    
    M0 = B0*RE**3
    D_cart = np.array([(L1 - g11*E)/(3*B0**2), (L2 - h11*E)/(3*B0**2), (L0 - g10*E)/(3*B0**2)])*RE
    m_cart = -1/B0*np.array([g11, h11, g10])
    c = np.roots([np.sum(m_cart**2), np.sum(2*D_cart*m_cart), np.sum(D_cart**2) - RE**2])
    rN = coordtransf.cart2spher(D_cart + c[1]*m_cart)
    rS = coordtransf.cart2spher(D_cart + c[0]*m_cart)
    
    return M0, D_cart, rN, rS

######################################################################################################################################################

def MagneticCoordinates(r, D_cart, rN):
    """
    Geomagnetic coordinates function. Returns the given position vectors in spherical magnetic coordinates according to the given Schmidt's eccentric
    dipole field model.
    Input:
        - r:      position vectors [spherical geographic] [m, rad, rad] (2D array of floats: [t, [r, theta, phi]])
        - D_cart: eccentric dipole centre position vector [Cartesian geographic] [m, m, m] (1D array of floats: [xD, yD, zD])
        - rN:     eccentric dipole North pole position vector [spherical geographic] [m, rad, rad] (1D array of floats: [rN, thetaN, phiN])
    Output:
        - rm:     position vectors [spherical magnetic] [m, rad, rad] (2D array of floats: [t, [rm, thetam, phim]])
    """
    _, thetaN, phiN = rN
    rm = np.zeros(r.shape)
    for j in range(r.shape[0]):
        r_cart = coordtransf.spher2cart(r[j])
        rm_cart = coordtransf.GEOtoMAG(r_cart - D_cart, thetaN, phiN)
        rm[j] = coordtransf.cart2spher(rm_cart)
    
    return rm

######################################################################################################################################################

def MagneticCutoff(M0, rm, lambdam, stormy):
    """
    Magnetic attenuation function. Returns the geomagnetic rigidity cut-off in function of particle arrival angle at the given geomagnetic position
    using Schmidt's eccentric dipole model with Stormer's geomagnetic shielding theory and SPENVIS' stormy magnetosphere correction.
    Input:
        - M0:      geomagnetic dipole moment [T m3] (float)
        - rm:      radial distance [spherical magnetic] [m] (float)
        - lambdam: latitude [spherical magnetic] [m] (float)
        - stormy:  stormy magnetosphere? (boolean)
    Output:
        - theta:  arrival angles (1D array of floats)
        - Pc:     cut-off rigidity [MeV/c/e] in function of arrival angle (1D array of floats [theta])
    """
    
    "Arrival angle"
    theta = np.linspace(-math.pi/2, math.pi/2, 100)
    
    "Cut-off rigidity (with stormy magnetosphere correction)"
    Pc0  = 7.39e-12*M0/(rm/6371.2e3)**2*math.cos(lambdam)**4/(1 + np.sqrt(1 - np.sin(theta)*math.cos(lambdam)**3))**2 # [MeV/c/e]
    fstorm = 1 - 0.54*np.exp(-Pc0/2900) if stormy else 1.
    Pc = Pc0*fstorm
    
    return theta, Pc

######################################################################################################################################################

def MagneticField(r, field, fieldarg=None):
    """
    Geomagnetic field function. Returns the magnetic flux density (B-field) in spherical geographic coordinates (Br, Btheta, Bphi) at the given
    positions according to the given magnetic field model.
    Input:
        - r:        position vectors [spherical geographic] [m, rad, rad] (2D array of floats: [t, [r, theta, phi]])
        - field:    magnetic field model (function)
        - fieldarg: input of magnetic field model (function argument)
    Output:
        - B_:       magnetic flux density vector [T] (2D array of floats: [t, [Br, Btheta, Bphi]])
    """
    r_RE, theta, phi = r[:,0]/6371.2e3, r[:,1], r[:,2]
    n, m, gnm, hnm = field() if fieldarg == None else field(fieldarg)
    B_ = np.zeros(r.shape)
    for j in range(r.shape[0]):
        Lnm, dLnm = special.lpmn(m[-1], n[-1], math.cos(theta[j]))
        Pnm, dPnm = np.zeros(len(n)), np.zeros(len(n))
        for i in range(len(n)):
            epsilonm = 1 if m[i] == 0 else 2
            E = math.sqrt(epsilonm*math.factorial(n[i] - m[i])/math.factorial(n[i] + m[i]))
            Pnm[i], dPnm[i] = E*Lnm[m[i],n[i]]*(-1)**m[i], E*dLnm[m[i],n[i]]*(-1)**m[i] # SciPy's Legendre polynomial has extra factor (-1)**m
            Br = (n[i] + 1)*(r_RE[j])**(-n[i] - 2)*Pnm[i]*(gnm[i]*math.cos(m[i]*phi[j]) + hnm[i]*math.sin(m[i]*phi[j]))
            if abs(theta[j]) == math.pi or theta[j] == 0:
                Btheta = 0.
                Bphi = np.nan
            else:
                Btheta = (r_RE[j])**(-n[i] - 2)*dPnm[i]*math.sin(theta[j])*(gnm[i]*math.cos(m[i]*phi[j]) + hnm[i]*math.sin(m[i]*phi[j]))
                Bphi = -(r_RE[j])**(-n[i] - 2)*Pnm[i]/math.sin(theta[j])*m[i]*(hnm[i]*math.cos(m[i]*phi[j]) - gnm[i]*math.sin(m[i]*phi[j]))
            B_[j] += np.array([Br, Btheta, Bphi])
    
    return B_

######################################################################################################################################################